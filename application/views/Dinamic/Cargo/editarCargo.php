<?php
defined('BASEPATH') OR exit('No direct script access allowed');
if($this->session->userdata('Perfil')==''){
	redirect(base_url());
}
?>
<?php 
$submit = array('name' => 'remember', 'class'=>'btn btn-primary btn-lg1 btn-block','value' => 'Actualizar');
$form = array('class' => 'login');
?>
<!-- banner -->
  <div class="courses_banner">
  	<div class="container">
  		<h3>Modificar Cargo</h3>
  		<p class="description"></p>
        <br>
        <div class="breadcrumb1">
            <ul>
                <li class="icon6"><a href="<?= base_url();?>">Home</a></li>
                <li class="current-page">Modificar Cargo</li>
            </ul>
        </div>
  	</div>
  </div>
    <!-- //banner -->
	<div class="courses_box1">
	   <div class="container">
	   <?=form_open_multipart(base_url().'cargo/editar/',$form)?>
            <?php foreach($cargo1 as $cargo)
            { ?>
                <p class="lead">Modificar Cargo</p>
                <div class="form-group">
                <input type="text" class='form-control' autocomplete='off' name='CodigoCargo' placeholder= "Nick *" required value="<?= $cargo->CodigoCargo; ?>" disabled="false">
                  </div>
                  <div class="form-group">
                <input type="hidden" class='form-control' autocomplete='off' name='CodigoCargo' placeholder= "Nick *" required value="<?= $cargo->CodigoCargo; ?>">
                  </div>
                <div class="form-group">
                	Nombre: <input type="text" class='form-control' autocomplete='off' name='Nombre' placeholder= "Nombre *" required value='<?= $cargo->Nombre ?>'>
                </div>
                <div class="form-group">
                    <?=form_submit($submit)?>
                </div>
                <div class="form-group">
                    <a href="<?=base_url()?>cargo/buscar" class="btn btn-primary btn-lg1 btn-block">Retroceder</a>
                </div>
                <?php if(!empty($error))
                {?>
                <div class="form-group">
                 <?= $error?>
		   		</div>
		   		<?php } 
		   	}?>
	   </div>
	</div>