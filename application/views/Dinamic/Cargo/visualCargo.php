<?php
defined('BASEPATH') OR exit('No direct script access allowed');
if($this->session->userdata('Perfil')==''){
	redirect(base_url());
}
?>
<!-- banner -->
  <div class="courses_banner">
  	<div class="container">
  		<h3>Vista de Cargo</h3>
  		<p class="description"></p>
        <br>
        <div class="breadcrumb1">
            <ul>
                <li class="icon6"><a href="<?= base_url();?>">Home</a></li>
                <li class="current-page">Vista de Cargo</li>
            </ul>
        </div>
  	</div>
  </div>
    <!-- //banner -->
	<div class="courses_box1">
	   <div class="container">
	       <div class="login">
                <p class="lead">Vista de Cargo</p>
                <?php foreach ($listado as $cargo) 
                {?>
                <div class="form-group">
                    Codigo: <input type="text" class='form-control' name='Codigo' placeholder= "Nick *" required value="<?=$cargo->CodigoCargo?>" disabled="false">
                  </div>
                <div class="form-group">
                  Nombre: <input type="text" class='form-control' name='Nombre' placeholder= "Nombre *" required value="<?=$cargo->Nombre?>" disabled="false">
                </div>
                <?php }?>
                <div class="form-group">
                    <a href="<?=base_url()?>cargo/buscar" class="btn btn-primary btn-lg1 btn-block">Retroceder</a>
                </div>
            </div>
	   </div>
	</div>