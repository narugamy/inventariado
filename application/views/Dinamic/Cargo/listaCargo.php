<?php
defined('BASEPATH') OR exit('No direct script access allowed');
if($this->session->userdata('Perfil')==''){
	redirect(base_url());
}
?>
<?php 
$submit = array('name' => 'remember', 'class'=>'btn btn-primary btn-lg1 btn-block','value' => 'Buscar');
$check = array('name' => 'submit','value' => 'true','required'=>'');
$form = array('class' => 'login');
?>
<!-- banner -->
  <div class="courses_banner">
  	<div class="container">
  		<h3>Lista Empleado</h3>
  		<p class="description"></p>
        <br>
        <div class="breadcrumb1">
            <ul>
                <li class="icon6"><a href="<?= base_url();?>">Home</a></li>
                <li class="current-page">Lista Empleado</li>
            </ul>
        </div>
  	</div>
  </div>
    <!-- //banner -->
	<div class="courses_box1">
		<div class="container">
	   	<div class="logi">
	    	<h3>Lista de Cargos Encontrados</h3>
	    	<?php
           	if(empty($cargo)==1)
           	{
				echo('No se ha encontrado ninguna cargo');
			}else{
            	foreach($cargo as $car)
            	{ ?>
	                <a href="<?=base_url()?>cargo/cargo/<?= $car->CodigoCargo?>" class="btn btn-success"><?= $car->Nombre?></a> 
	                <a href="<?= base_url()?>cargo/editar/<?=$car->CodigoCargo;?>" class="btn btn-warning" role="buttohon">Editar</a>
	                <?php if($car->Estado==1)
	                {?>
	                <a href="<?= base_url()?>cargo/eliminar/<?=$car->CodigoCargo;?>" class="btn btn-danger" role="button">Eliminar</a><br/><br>
	                <?php }else{?>
					<a href="<?= base_url()?>cargo/habilitar/<?=$car->CodigoCargo;?>" class="btn btn-primary" role="button">Habilitar</a><br/><br>
					<?php } ?>
      		<?php }
      		}?>
            <hr>
            <a href="<?=base_url()?>cargo/buscar" class="btn btn-success">Retroceder</a> 
	   </div>
	   </div>
	</div>