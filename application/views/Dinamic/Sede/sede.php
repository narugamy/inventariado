<?php
defined('BASEPATH') OR exit('No direct script access allowed');
if($this->session->userdata('Perfil')==''){
	redirect(base_url());
}
?>
<!-- banner -->
  <div class="courses_banner">
  	<div class="container">
  		<h3>Administrar Sede</h3>
  		<p class="description"></p>
        <br>
        <div class="breadcrumb1">
            <ul>
                <li class="icon6"><a href="<?= base_url();?>">Home</a></li>
                <li class="current-page">Administrar Sede</li>
            </ul>
        </div>
  	</div>
  </div>
    <!-- //banner -->
	<div class="courses_box1">
	    <div class="container">
            <div class="login">
                <h3>Listado de Opciones</h3>
                <div class="form-group">
                    <a href="<?=base_url()?>sede/registrar" class="btn btn-primary btn-lg1 btn-block">Registrar Nueva Sede</a>
                </div>
                <div class="form-group">
                    <a href="<?=base_url()?>sede/buscar" class="btn btn-primary btn-lg1 btn-block">Buscar Sede</a>
                </div>
            </div>
	   </div>
	</div>