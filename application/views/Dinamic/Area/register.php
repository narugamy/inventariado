<?php
defined('BASEPATH') OR exit('No direct script access allowed');
if($this->session->userdata('Perfil')==''){
	redirect(base_url());
}
?>
<?php 
$submit = array('name' => 'remember', 'class'=>'btn btn-primary btn-lg1 btn-block','value' => 'Register');
$check = array('name' => 'submit','value' => 'true','required'=>'');
$form = array('class' => 'login');
?>
<!-- banner -->
  <div class="courses_banner">
  	<div class="container">
  		<h3>Registro</h3>
  		<p class="description"></p>
        <br>
        <div class="breadcrumb1">
            <ul>
                <li class="icon6"><a href="<?= base_url();?>">Home</a></li>
                <li class="current-page">Registro</li>
            </ul>
        </div>
  	</div>
  </div>
    <!-- //banner -->
	<div class="courses_box1">
	   <div class="container">
	   <?=form_open_multipart(base_url().'area/register',$form)?>
                <p class="lead">Registro de nuevo usuario</p>
                <div class="form-group">
                  <input type="text" class='form-control' name='Nombre' placeholder= "Nombre *" required value='' autocomplete="off">
                </div>

                <div class="form-group">
                    <?=form_submit($submit)?>
                </div>
                <div class="form-group">
                    <a href="<?=base_url()?>cargo" class="btn btn-primary btn-lg1 btn-block">Retroceder</a>
                </div>
                <?php if(!empty($error)){?>
                <div class="form-group">
                 <?= $error?>
		   		</div>
		   		<?php } ?>
	   </div>
	</div>