<?php
defined('BASEPATH') OR exit('No direct script access allowed');
if($this->session->userdata('Perfil')==''){
	redirect(base_url());
}
?>
<?php 
$submit = array('name' => 'remember', 'class'=>'btn btn-primary btn-lg1 btn-block','value' => 'Actualizar');
$form = array('class' => 'login');
?>
<!-- banner -->
  <div class="courses_banner">
  	<div class="container">
  		<h3>Modificar Area</h3>
  		<p class="description"></p>
        <br>
        <div class="breadcrumb1">
            <ul>
                <li class="icon6"><a href="<?= base_url();?>">Home</a></li>
                <li class="current-page">Modificar Area</li>
            </ul>
        </div>
  	</div>
  </div>
    <!-- //banner -->
	<div class="courses_box1">
	   <div class="container">
	   <?=form_open_multipart(base_url().'area/editar/',$form)?>
            <?php foreach($area1 as $area)
            { ?>
                <p class="lead">Modificar Area</p>
                <div class="form-group">
                <input type="text" class='form-control' autocomplete='off' name='CodigoArea' placeholder= "Nick *" required value="<?= $area->CodigoArea; ?>" disabled="false">
                  </div>
                  <div class="form-group">
                <input type="hidden" class='form-control' autocomplete='off' name='CodigoArea' placeholder= "Nick *" required value="<?= $area->CodigoArea; ?>">
                  </div>
                <div class="form-group">
                	Nombre: <input type="text" class='form-control' autocomplete='off' name='Nombre' placeholder= "Nombre *" required value='<?= $area->Nombre ?>'>
                </div>
                <div class="form-group">
                    <?=form_submit($submit)?>
                </div>
                <div class="form-group">
                    <a href="<?=base_url()?>area/buscar" class="btn btn-primary btn-lg1 btn-block">Retroceder</a>
                </div>
                <?php if(!empty($error))
                {?>
                <div class="form-group">
                 <?= $error?>
		   		</div>
		   		<?php } 
		   	}?>
	   </div>
	</div>