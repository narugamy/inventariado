<?php
defined('BASEPATH') OR exit('No direct script access allowed');
if($this->session->userdata('Perfil')==''){
	redirect(base_url());
}
?>
<?php 
$submit = array('name' => 'remember', 'class'=>'btn btn-primary btn-lg1 btn-block','value' => 'Buscar');
$check = array('name' => 'submit','value' => 'true','required'=>'');
$form = array('class' => 'login');
?>
<!-- banner -->
  <div class="courses_banner">
  	<div class="container">
  		<h3>Lista Area</h3>
  		<p class="description"></p>
        <br>
        <div class="breadcrumb1">
            <ul>
                <li class="icon6"><a href="<?= base_url();?>">Home</a></li>
                <li class="current-page">Lista Area</li>
            </ul>
        </div>
  	</div>
  </div>
    <!-- //banner -->
	<div class="courses_box1">
		<div class="container">
	   	<div class="logi">
	    	<h3>Lista de Sedes Encontradas</h3>
	    	<?php
           	if(empty($area)==1)
           	{
				echo('No se ha encontrado ninguna cargo');
			}else{
            	foreach($area as $are)
            	{ ?>
	                <a href="<?=base_url()?>area/area/<?= $are->CodigoArea?>" class="btn btn-success"><?= $are->Nombre?></a> 
	                <a href="<?= base_url()?>area/editar/<?=$are->CodigoArea;?>" class="btn btn-warning" role="buttohon">Editar</a>
	                <?php if($are->Estado==1)
	                {?>
	                <a href="<?= base_url()?>area/eliminar/<?=$are->CodigoArea;?>" class="btn btn-danger" role="button">Eliminar</a><br/><br>
	                <?php }else{?>
					<a href="<?= base_url()?>area/habilitar/<?=$are->CodigoArea;?>" class="btn btn-primary" role="button">Habilitar</a><br/><br>
					<?php } ?>
      		<?php }
      		}?>
            <hr>
            <a href="<?=base_url()?>area/buscar" class="btn btn-success">Retroceder</a> 
	   </div>
	   </div>
	</div>