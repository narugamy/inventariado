<?php
defined('BASEPATH') OR exit('No direct script access allowed');
if($this->session->userdata('Perfil')==''){
	redirect(base_url());
}
?>
<?php 
$submit = array('name' => 'remember', 'class'=>'btn btn-primary btn-lg1 btn-block','value' => 'Buscar');
$check = array('name' => 'submit','value' => 'true','required'=>'');
$form = array('class' => 'login');
?>
<!-- banner -->
  <div class="courses_banner">
  	<div class="container">
  		<h3>Buscar Area</h3>
  		<p class="description"></p>
        <br>
        <div class="breadcrumb1">
            <ul>
                <li class="icon6"><a href="<?= base_url();?>">Home</a></li>
                <li class="current-page">Buscar Area</li>
            </ul>
        </div>
  	</div>
  </div>
    <!-- //banner -->
	<div class="courses_box1">
	   <div class="container">
	   <?=form_open_multipart(base_url().'area/buscarArea',$form)?>
                <p class="lead">Buscar Area</p>
                <div class="form-group">
                  Nombre: <input type="text" class='form-control' autocomplete='off' name='Nombre' placeholder= "Nombre *" value=''>
                </div>
                <div class="form-group">
                    <?=form_submit($submit)?>
                </div>
                <div class="form-group">
                    <a href="<?=base_url()?>area" class="btn btn-primary btn-lg1 btn-block">Retroceder</a>
                </div>
                <?php if(!empty($error)){?>
                <div class="form-group">
                 <?= $error?>
		   		</div>
		   		<?php } ?>
	   </div>
	</div>