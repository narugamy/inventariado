<?php
defined('BASEPATH') OR exit('No direct script access allowed');
if($this->session->userdata('Perfil')==''){
	redirect(base_url());
}
?>
<?php 
$submit = array('name' => 'remember', 'class'=>'btn btn-primary btn-lg1 btn-block','value' => 'Register');
$check = array('name' => 'submit','value' => 'true','required'=>'');
$form = array('class' => 'login');
?>
<!-- banner -->
  <div class="courses_banner">
  	<div class="container">
  		<h3>Registro</h3>
  		<p class="description"></p>
        <br>
        <div class="breadcrumb1">
            <ul>
                <li class="icon6"><a href="<?= base_url();?>">Home</a></li>
                <li class="current-page">Registro</li>
            </ul>
        </div>
  	</div>
  </div>
    <!-- //banner -->
	<div class="courses_box1">
	   <div class="container">
	   <?=form_open_multipart(base_url().'Empleado/register',$form)?>
                <p class="lead">Registro de nuevo usuario</p>
                <div class="form-group">
                    Codigo: <input type="text" class='form-control' name='Codigo' placeholder= "Nick *" required value="" autocomplete="off">
                </div>
                <div class="form-group">
                    <input type="hidden" name="Password" value="41cd345bc11a1aa229024689816df668">
                </div>
                <div class="form-group">
                  Nombre: <input type="text" class='form-control' name='Nombre' placeholder= "Nombre *" required value='' autocomplete="off">
                </div>
                <div class="form-group">
                    Apellidos: <input type="text" class='form-control' name='Apellidos' placeholder= "Apellidos *" required value='' autocomplete="off">
                </div>
                <div class="form-group" autocomplete="off">
                   Genero: <select name="Genero" class='form-control' required>
                    	<option value="">Escoja un Genero</option>
                        <option value="M">Masculino</option>
                        <option value="F">Femenino</option>
                    </select>
                </div>
                <div class="form-group">
                    Email: <input type="email" class='form-control' name='Email' placeholder= "Email *" required value='' autocomplete="off">
                </div>
                <div class="form-group">
                    Dni: <input type="number" class='form-control' name='Dni' placeholder= "Dni *" required value='' autocomplete="off">
                </div>
                <div class="form-group">
                    Direccion: <input type="text" class='form-control' name='Direccion' placeholder= "Direccion *" required value='' autocomplete="off">
                </div>
                <div class="form-group">
                    Telefono: <input type="number" class='form-control' name='Telefono' placeholder= "telefono *" required value='' autocomplete="off">
                </div>
                <div class="form-group">
                    Fecha de Nacimiento: <input type="date" class='form-control' name='Fecha_Nacimiento' placeholder= "Fecha_Nacimiento *" required value='' autocomplete="off">
                </div>
                <div class="form-group">
                    Fecha de Ingreso: <input type="date" class='form-control' name='Fecha_Ingreso' placeholder= "Fecha_Ingreso *" required value='' autocomplete="off">
                </div>
                <div class="form-group">
                    Imagen: <input type="file" class='form-control' name='userfile' placeholder= "Foto" required value='' autocomplete="off">
                </div>
                <div class="form-group">
                Cargo:  <select name="CodigoCargo" class='form-control' autocomplete="off">
                        <option value="">------------Escoja un Cargo------------</option>
                        <?php foreach($cargo1 as $cargo)
                        { ?>
                        <option value="<?=$cargo->CodigoCargo?>"><?=$cargo->Nombre?></option>
                        <?php }?>
                    </select>
                </div>
                <div class="form-group">
                Sede:   <select name="CodigoSede" class='form-control' autocomplete="off">
                        <option value="">------------Escoja una Sede------------</option>
                        <?php foreach($sede1 as $sede)
                        { ?>
                        <option value="<?=$sede->CodigoSede?>"><?=$sede->Nombre?></option>
                        <?php }?>
                    </select>
                </div>
                <div class="form-group">
                <label for="Ruta">Foto</label>
                <input type="file" name="Ruta" class='users form-control' placeholder='Img' value="" id="Ruta">
                <p id="im"></p>
            </div>
                <div class="form-group">
                    <?=form_submit($submit)?>
                </div>
                <div class="form-group">
                    <a href="<?=base_url()?>empleado" class="btn btn-primary btn-lg1 btn-block">Retroceder</a>
                </div>
                <?php if(!empty($error)){?>
                <div class="form-group">
                 <?= $error?>
		   		</div>
		   		<?php } ?>
	   </div>
	</div>