<?php
defined('BASEPATH') OR exit('No direct script access allowed');
if($this->session->userdata('Perfil')==''){
	redirect(base_url());
}
?>
<?php 
$submit = array('name' => 'remember', 'class'=>'btn btn-primary btn-lg1 btn-block','value' => 'Buscar');
$check = array('name' => 'submit','value' => 'true','required'=>'');
$form = array('class' => 'login');
?>
<!-- banner -->
  <div class="courses_banner">
  	<div class="container">
  		<h3>Buscar Empleado</h3>
  		<p class="description"></p>
        <br>
        <div class="breadcrumb1">
            <ul>
                <li class="icon6"><a href="<?= base_url();?>">Home</a></li>
                <li class="current-page">Buscar Empleado</li>
            </ul>
        </div>
  	</div>
  </div>
    <!-- //banner -->
	<div class="courses_box1">
	   <div class="container">
	   <?=form_open_multipart(base_url().'empleado/buscarempleado',$form)?>
                <p class="lead">Buscar Empleado</p>
                <div class="form-group">
                	Nombre: <input type="text" class='form-control' autocomplete='off' name='Nombre' placeholder= "Nombre *" value=''>
                </div>
                <div class="form-group">
                    Apellidos: <input type="text" class='form-control' autocomplete='off' name='Apellidos' placeholder= "Apellidos *" value=''>
                </div>
                <div class="form-group">
                Cargo:  <select name="Cargo" class="form-control">
                            <option value="" selected>----------</option>
                            <?php foreach($cargo1 as $cargo)
                            { ?>
                            <option value="<?=$cargo->CodigoCargo?>"><?=$cargo->Nombre?></option>
                            <?php }?>
                        </select>
                </div>
                <div class="form-group">
                Sede:   <select name="Sede" class='form-control'>
                            <option value="">----------</option>
                            <?php foreach($sede1 as $sede)
                            { ?>
                            <option value="<?=$sede->CodigoSede?>"><?=$sede->Nombre?></option>
                            <?php }?>
                        </select>
                </div>
                <div class="form-group">
                Area:   <select name="Area" class='form-control'>
                            <option value="">----------</option>
                            <?php foreach($area1 as $area)
                            { ?>
                            <option value="<?=$area->CodigoArea?>"><?=$area->Nombre?></option>
                            <?php }?>
                        </select>
                </div>
                <div class="form-group">
                    <?=form_submit($submit)?>
                </div>
                <div class="form-group">
                    <a href="<?=base_url()?>empleado" class="btn btn-primary btn-lg1 btn-block">Retroceder</a>
                </div>
                <?php if(!empty($error)){?>
                <div class="form-group">
                 <?= $error?>
		   		</div>
		   		<?php } ?>
	   </div>
	</div>