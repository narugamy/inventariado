<?php
defined('BASEPATH') OR exit('No direct script access allowed');
if($this->session->userdata('Perfil')==''){
	redirect(base_url());
}
?>
<?php 
$submit = array('name' => 'remember', 'class'=>'btn btn-primary btn-lg1 btn-block','value' => 'Actualizar');
$check = array('name' => 'submit','value' => 'true','required'=>'');
$form = array('class' => 'login');
?>
<!-- banner -->
  <div class="courses_banner">
  	<div class="container">
  		<h3>RegisModificar Empleadotro</h3>
  		<p class="description"></p>
        <br>
        <div class="breadcrumb1">
            <ul>
                <li class="icon6"><a href="<?= base_url();?>">Home</a></li>
                <li class="current-page">Modificar Empleado</li>
            </ul>
        </div>
  	</div>
  </div>
    <!-- //banner -->
	<div class="courses_box1">
	   <div class="container">
	   <?=form_open_multipart(base_url().'empleado/editar',$form)?>
            <?php foreach($datos as $empleado)
            { ?>
                <p class="lead">Modificar empleado</p>
                <div class="form-group">
                    Codigo:<input type="text" class='form-control' autocomplete='off' name='Codigo' placeholder= "Nick *" required value="<?= $empleado->Codigo ?>" disabled="false">
                  </div>
                 <div class="form-group">
                    <input type="text" class='form-control' autocomplete='off' name='Codigo' placeholder= "Nick *" required value="<?= $empleado->Codigo ?>">
                  </div>
                <div class="form-group">
                  Nombre: <input type="text" class='form-control' autocomplete='off' name='Nombre' placeholder= "Nombre *" required value='<?= $empleado->Nombre ?>' disabled="false">
                </div>
                <div class="form-group">
                    Apellidos: <input type="text" class='form-control' autocomplete='off' name='Apellidos' placeholder= "Apellidos *" required value='<?= $empleado->Apellidos ?>' disabled="false">
                </div>
                <div class="form-group">
                    Email: <input type="email" class='form-control' autocomplete='off' name='Email' placeholder= "<?= $empleado->Email ?>" required value=''>
                </div>
                <div class="form-group">
                    Dni: <input type="number" class='form-control' autocomplete='off' name='Dni' placeholder= "<?= $empleado->Dni ?>" required value=''>
                </div>
                <div class="form-group">
                    Direccion: <input type="text" class='form-control' autocomplete='off' name='Direccion' placeholder= "<?= $empleado->Direccion ?>" required value=''>
                </div>
                <div class="form-group">
                    Telefono:<input type="number" class='form-control' autocomplete='off' name='Telefono' placeholder= "<?= $empleado->Telefono ?>" required value=''>
                </div>
                <div class="form-group">
                    Fecha de Nacimiento<input type="date" class='form-control' autocomplete='off' name='Fecha_Nacimiento' placeholder= "Fecha_Nacimiento *" required value="<?= $empleado->Fecha_Nacimiento ?>" disabled="false">
                </div>
                <div class="form-group">
                    Fecha de Ingreso<input type="date" class='form-control' autocomplete='off' name='Fecha_Ingreso' placeholder= "Fecha_Ingreso *" required value="<?= $empleado->Fecha_Ingreso ?>" disabled="false">
                </div>
       	         <div class="form-group">
                Cargo: <select name="CodigoCargo" class="form-control">
                            <option value="" selected>----------</option>
                            <?php foreach($cargo1 as $cargo)
                            { ?>
                            <option value="<?=$cargo->CodigoCargo?>"><?=$cargo->Nombre?></option>
                            <?php }?>
                        </select>
                </div>
                <div class="form-group"> 
                Area: <select name="CodigoArea" class='form-control'>
                            <option value="">----------</option>
                            <?php foreach($area1 as $area)
                            { ?>
                            <option value="<?=$area->CodigoArea?>"><?=$area->Nombre?></option>
                            <?php }?>
                        </select>
                </div>
                <?php } ?>
                <div class="form-group">
                    <?=form_submit($submit)?>
                </div>
                <div class="form-group">
                    <a href="<?=base_url()?>empleado/buscar" class="btn btn-primary btn-lg1 btn-block">Retroceder</a>
                </div>
                <?php if(!empty($error))
                {?>
                <div class="form-group">
                 <?= $error?>
		   		</div>
		   		<?php } ?>
	   </div>
	</div>