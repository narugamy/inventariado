<?php
defined('BASEPATH') OR exit('No direct script access allowed');
if($this->session->userdata('Perfil')==''){
	redirect(base_url());
}
?>
<!-- banner -->
  <div class="courses_banner">
  	<div class="container">
  		<h3>Vista de Empleado</h3>
  		<p class="description"></p>
        <br>
        <div class="breadcrumb1">
            <ul>
                <li class="icon6"><a href="<?= base_url();?>">Home</a></li>
                <li class="current-page">Vista de Empleado</li>
            </ul>
        </div>
  	</div>
  </div>
    <!-- //banner -->
	<div class="courses_box1">
	   <div class="container">
	       <div class="login">
                <p class="lead">Vista de Empleado</p>
                <?php foreach ($listado as $empleado) 
                {?>
                <div class="form-group">
                    Codigo: <input type="text" class='form-control' name='Codigo' placeholder= "Nick *" required value="<?=$empleado->Codigo?>" disabled="false">
                  </div>
                <div class="form-group">
                  Nombre: <input type="text" class='form-control' name='Nombre' placeholder= "Nombre *" required value="<?=$empleado->Nombre?>" disabled="false">
                </div>
                <div class="form-group">
                    Apellidos: <input type="text" class='form-control' name='Apellidos' placeholder= "Apellidos *" required value="<?=$empleado->Apellidos?>" disabled="false">
                </div>
                <div class="form-group">
                   Genero: <input type="text" class='form-control' name="Genero" value="<?=$empleado->Genero?>" disabled="false" placeholder="Genero" >
                </div>
                <div class="form-group">
                    Email: <input type="email" class='form-control' name='Email' placeholder= "Email *" required value="<?=$empleado->Email?>" disabled="false">
                </div>
                <div class="form-group">
                    Dni: <input type="number" class='form-control' name='Dni' placeholder= "Dni *" required value="<?=$empleado->Dni?>" disabled="false">
                </div>
                <div class="form-group">
                    Dirección: <input type="text" class='form-control' name='Direccion' placeholder= "Direccion *" required value="<?=$empleado->Direccion?>" disabled="false">
                </div>
                <div class="form-group">
                    Telefono: <input type="number" class='form-control' name='Telefono' placeholder= "telefono *" required value="<?=$empleado->Telefono?>" disabled="false">
                </div>
                <div class="form-group">
                    Fecha de Nacimiento: <input type="date" class='form-control' name='Fecha_Nacimiento' placeholder= "Fecha_Nacimiento *" required value="<?=$empleado->Fecha_Nacimiento?>" disabled="false">
                </div>
                <div class="form-group">
                    Fecha de Ingreso: <input type="date" class='form-control' name='Fecha_Ingreso' placeholder= "Fecha_Ingreso *" required value="<?=$empleado->Fecha_Ingreso?>" disabled="false">
                </div>
                <div class="form-group">
                    <figure>
                        <img src="<?=base_url()?>assets/images/<?=$empleado->Ruta?>" alt="img">
                    </figure>
                </div>
                <?php }?>
                <div class="form-group">
                    <a href="<?=base_url()?>empleado/buscar" class="btn btn-primary btn-lg1 btn-block">Retroceder</a>
                </div>
            </div>
	   </div>
	</div>