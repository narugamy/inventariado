<?php
defined('BASEPATH') OR exit('No direct script access allowed');
if($this->session->userdata('Perfil')==''){
	redirect(base_url());
}
?>
<?php 
$submit = array('name' => 'remember', 'class'=>'btn btn-primary btn-lg1 btn-block','value' => 'Buscar');
$check = array('name' => 'submit','value' => 'true','required'=>'');
$form = array('class' => 'login');
?>
<!-- banner -->
  <div class="courses_banner">
  	<div class="container">
  		<h3>Lista Empleado</h3>
  		<p class="description"></p>
        <br>
        <div class="breadcrumb1">
            <ul>
                <li class="icon6"><a href="<?= base_url();?>">Home</a></li>
                <li class="current-page">Lista Empleado</li>
            </ul>
        </div>
  	</div>
  </div>
    <!-- //banner -->
	<div class="courses_box1">
		<div class="container">
	   		<div class="login">
	       		<h3>Lista de Empleados Encontrados</h3>
	            <?php
	           	if(empty($empleado)==1)
	           	{
					echo('No se ha encontrado ninguna empleado');
				}else{
	            	foreach($empleado as $empl)
	            	{ ?>
	            	<div class="form-group">
		                <a href="<?=base_url()?>empleado/personal/<?= $empl->Codigo?>" class="btn btn-success"><?php echo $empl->Nombre."  ".$empl->Apellidos?></a> 
		                <a href="<?= base_url()?>empleado/editar/<?=$empl->Codigo;?>" class="btn btn-warning" role="buttohon">Editar</a>
		                <a href="<?= base_url()?>empleado/eliminar/<?=$empl->Codigo;?>" class="btn btn-danger" role="button">Eliminar</a>
		            </div>
	          	<?php }
	          	} ?>
	            <hr>
	            <a href="<?=base_url()?>empleado/buscar/" class="btn btn-primary btn-lg1 btn-block">Retroceder</a>
           </div>
	   </div>
	</div>