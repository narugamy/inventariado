<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<div class="courses_banner">
  	<div class="container">
  		<h3>Noticias</h3>
  		<p class="description"></p>
  		<br />
        <div class="breadcrumb1">
            <ul>
                <li class="icon6"><a href="<?= base_url();?>">Home</a></li>
                <li class="current-page">Noticias</li>
            </ul>
        </div>
  	</div>
  </div>
    <!-- //banner -->
	<div class="services">
	   <div class="container">
	   	  <h1>Noticias</h1>
	   	  <div class="service_box1">
	   	    <div class="col-md-6">
                <div class="service_1">
                    <div class="service_1-left">
                       <span class="icon_5"><i class="fa fa-star"> </i></span>
                    </div>
                    <div class="service_1-right">
                       <h5><a href="#">MUDANZA A PLANTA ROCA</a></h5>
                       <p>En el mes de mayo Urbano Argentina inauguró su nueva planta de procesamiento postal y logístico y sede central en la Ciudad de Buenos Aires continuando así con su expansión en el mercado argentino. La planta de 9.000 m2 ubicada en el barrio porteño de Villa Lugano funciona como Casa Central, Centro de Tratamiento Postal, Centro de Tratamiento de Logística Liviana, WareHousing, Courier Internacional y tercer sucursal de la Ciudad de Buenos Aires, la cual se sumó a las 62 sucursales de la compañía a lo largo y ancho del país. El cambio implico una notable mejora de la capacidad operativa de la compañía, una gran evolución tecnológica y se tradujo en una inmediata mejora en el servicio prestado a nuestros clientes, en las condiciones laborales del equipo de Urbano Argentina y en la relación estratégica con nuestros proveedores y partners. Marcelo Wirth, Director General de Urbano Argentina, afirmó: “La inauguración de nuestra nueva planta es un paso adelante en la consolidación de Urbano como empresa líder en el mercado postal y logístico en la Argentina y una notable mejora en el servicio prestado a nuestros clientes”.</p>
                    </div>
                </div>
            </div>
             <div class="col-md-6">
                <div class="service_1">
                    <div class="service_1-left">
                       <span class="icon_5"><i class="fa fa-star"> </i></span>
                    </div>
                    <div class="service_1-right">
                       <h5><a href="#">APERTURA DE AGENTE AUTORIZADO EN CHALATENANGO</a></h5>
                       <p>Aperturamos y ubicamos Agentes autorizados para la recepción de envíos internacionales en puntos estratégicos del departamento de Chalatenango, con la finalidad de solventar la necesidad de nuestros clientes en los envíos de documentos dirigidos hacia el exterior, tales como Partidas de Nacimiento, Pasaportes, Notificaciones, entre otros. El servicio prestado incluye entregas hacia cualquier parte del mundo</p>
                    </div>
                </div>
            </div>
            <div class="clearfix"> </div>
	   </div>
	   <div class="service_box1">
	   	    <div class="col-md-6">
                <div class="service_1">
                    <div class="service_1-left">
                       <span class="icon_5"><i class="fa fa-star"> </i></span>
                    </div>
                    <div class="service_1-right">
                       <h5><a href="#">RECONOCIMIENTO</a></h5>
                       <p>Semanas atrás dos Bancos tradicionales de la ciudad Unibanco y Solidario fusionaron sus operaciones. Este evento genero una serie de retrasos en todos sus procesos, uno de ellos la generación de información de los estados de cuenta para sus clientes. Urbano aporto con su conocimiento y contingente para mitigar el retraso generado. Esta semana nos visitaron funcionarios de esta institución con el objeto de expresar su reconocimiento y agradecimiento por el apoyo efectivo que recibieron de parte de Urbano en esta transición.</p>
                    </div>
                </div>
            </div>
             <div class="col-md-6">
                <div class="service_1">
                    <div class="service_1-left">
                       <span class="icon_5"><i class="fa fa-star"> </i></span>
                    </div>
                    <div class="service_1-right">
                       <h5><a href="#">APERTURA DE CENTROS DE NEGOCIOS EN SUCURSALES PRICESMART METROCENTRO Y SANTA ELENA</a></h5>
                       <p>El pasado mes de Marzo aperturamos los Centros de Negocios ubicados en las sucursales de PriceSmart Metrocentro y Santa Elena, con los cuales ampliamos nuestra presencia y cobertura en la Zona Central de El Salvador. En los Centros de Negocios se reciben los paquetes que los clientes han adquirido y los cuales no pueden ser trasladados por sus propios medios. Damos cobertura a nivel nacional en la entrega de los bienes adquiridos y nos damos a conocer como la empresa líder en el transporte de encomiendas a nivel nacional. A través de este servicio tenemos la oportunidad de estar mas cerca de nuestro clientes, teniendo presencia no solo con sucursales propias sino creando un posicionamiento de marca mas fuerte ubicándonos en la mente de los consumidores como la empresa líder en entrega de correspondencia y logística liviana</p>
                    </div>
                </div>
            </div>
            <div class="clearfix"> </div>
	   </div>
     <div class="service_box1">
          <div class="col-md-6">
                <div class="service_1">
                    <div class="service_1-left">
                       <span class="icon_5"><i class="fa fa-star"> </i></span>
                    </div>
                    <div class="service_1-right">
                       <h5><a href="#">CAMPAÑA PARA ENVIOS MASIVOS HACIA EL EXTRANJERO</a></h5>
                       <p>URBANO promocionó la alianza estratégica con DHL para el envío de Documentos fuera del país. El objetivo principal de la campaña es hacerle ver a nuestros clientes lo importante que es enviar sus documentos de una forma segura y rápida, con una empresa responsable y que genere confianza. El servicio ofrece un programa de fidelidad, que busca generar más tráfico en nuestras agencias, y más ingresos para cada una de las sucursales. El programa consiste en: por cada tres envíos realizados el cliente recibe uno gratis. Esto sumado a nuestra excelente atención y gran calidad humana que se demuestra en la atención hacia nuestros clientes.</p>
                    </div>
                </div>
            </div>
             <div class="col-md-6">
                <div class="service_1">
                    <div class="service_1-left">
                       <span class="icon_5"><i class="fa fa-star"> </i></span>
                    </div>
                    <div class="service_1-right">
                       <h5><a href="#">URBANO COLABORA CON QUITO Y SU OFERTA CULTURAL</a></h5>
                      <p>Consientes de nuestra responsabilidad con la comunidad, Urbano inaugura un convenio de colaboración con la Fundación de Museos de Quito para facilitar procesos logísticos tanto en el transporte de las obras como en los procesos de convocatoria a las exposiciones. Estamos convencidos de que mientras más quiteños conozcan y disfruten de lo que los principales museos de la ciudad les ofrecen mejor será su calidad de vida, por eso ponemos toda nuestra capacidad y conocimiento a disposición de esta importante causa. Urbano colabora, por medio de la Fundación de Museo de Quito, con las exposiciones que se exhiben en todos los museos que gestiona esta importante institución, entre ellos el Centro de Arte Contemporáneo, Museo de la Ciudad, Centro de Arte Metropolitano, entre otros espacios públicos de gran trascendencia. Algunas de las exposiciones, en las que Urbano ha contribuido son la Muestra de Arte Mariano Aguilera y la exposición Alma Mía.</p>
                    </div>
                </div>
            </div>
            <div class="clearfix"> </div>
     </div>
	</div>
	</div>
	<div class="bg_2">
	  <div class="container">
	  	 <div class="col-md-6 service_2-left">
              <h2>Profesinales en nuestro trabajo</h2>
         </div>
         <div class="col-md-6 service_2-right">
            <p></p>
         </div>
         <div class="clearfix"> </div>
	  </div>
	</div>