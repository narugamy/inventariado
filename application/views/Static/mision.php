<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!-- banner -->
  <div class="courses_banner">
  	<div class="container">
  		<h3>Misión</h3>
  		<p class="description"></p>
  		<br />
        <div class="breadcrumb1">
            <ul>
                <li class="icon6"><a href="index.html">Home</a></li>
                <li class="current-page">Misión</li>
            </ul>
        </div>
  	</div>
  </div>
    <!-- //banner -->
	<div class="features">
	   <div class="container">
	   	  <h1>Misión</h1>
	   	  <h2>Misión</h2>
	   	  <p>"Crear un nuevo modelo de distribución masiva de correspondencia, siendo la alternativa y el punto de referencia en Latinoamérica.
Única red de distribución masiva de correo operando en Latinoamérica. Urbano ofrece entregas confiables y certificadas, con los mejores tiempos de tránsito, utilizando procesos automatizados y tecnología de punta."</p>
	  </div>
	</div>