<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<?php
$username = array('autocomplete'=>'off','name' => 'log_username', 'class'=>'users form-control','placeholder' => 'Nick','value' =>'','required'=>'');
$password = array('autocomplete'=>'off','name' => 'Password','class'=>'password required form-control','placeholder' => 'Password','value' => '','required'=>'');
$submit = array('name' => 'submit', 'class'=>'btn btn-primary btn-lg1 btn-block','value' => 'Log In');
$form = array('class' => 'login');
?>
<?php if(!$this->session->userdata('Perfil')=='')
	  { 
	  	redirect(base_url());?>	
<?php } ?>
<?php if($this->session->userdata('Perfil')=='')
	  {?>
<!-- banner -->
  <div class="courses_banner">
  	<div class="container">
  		<h3>Login</h3>
  		<p class="description">
             Bienvenidos al area de login para poder acceder a nuestras opciones y caracteristicas para veneficiar su trabajo y agilizar sus procesos como usuario a nuestra aplicacion.
        </p>
        <div class="breadcrumb1">
            <ul>
                <li class="icon6"><a href="<?= base_url();?>">Home</a></li>
                <li class="current-page">Login</li>
            </ul>
        </div>
  	</div>
  </div>
    <!-- //banner -->
	<div class="courses_box1">
	   <div class="container">
	   	<?=form_open(base_url().'home/nuevo',$form)?>
	    	<p class="lead">Bienvenido</p>
		    <div class="form-group">
			    <?=form_input($username)?>
		    </div>
		    <div class="form-group">
			    <?=form_password($password)?>
		    </div>
		    <?=form_hidden('token',$token)?>
		    <div class="form-group">
		    	<input type="checkbox" name="remember" value="true"> Recuerdame
		    	<?=form_submit($submit)?>
		    </div>
	    <?=form_close()?>
	   </div>
	</div>
	<?php }?>