<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<div class="courses_banner">
  	<div class="container">
  		<h3>Servcios</h3>
  		<p class="description"></p>
  		<br />
        <div class="breadcrumb1">
            <ul>
                <li class="icon6"><a href="<?= base_url();?>">Home</a></li>
                <li class="current-page">Servcios</li>
            </ul>
        </div>
  	</div>
  </div>
    <!-- //banner -->
	<div class="services">
	   <div class="container">
	   	  <h1>Servcios Brindados</h1>
	   	  <div class="service_box1">
	   	    <div class="col-md-6">
                <div class="service_1">
                    <div class="service_1-left">
                       <span class="icon_5"><i class="fa fa-users"> </i></span>
                    </div>
                    <div class="service_1-right">
                       <h5><a href="#">Postal</a></h5>
                       <p>• Estados de cuenta o cualquier otro documento informativo sin valor comercial que debe recibir una gran base de clientes.</p>
                       <p>• Entregamos, con el debido acuse de recibo, documentos que tienen un valor y que deben ser recibidos por un destinatario específico. Custodiamos el documento y lo distribuimos en tiempos precisos.</p>
                    </div>
                </div>
            </div>
             <div class="col-md-6">
                <div class="service_1">
                    <div class="service_1-left">
                       <span class="icon_5"><i class="fa fa-phone"> </i></span>
                    </div>
                    <div class="service_1-right">
                       <h5><a href="#">Home Delivery</a></h5>
                       <p>• Transportamos su mercadería desde sus instalaciones a un destino predeterminado.</p>
                       <p>• Garantizamos el cumpliendo de los tiempos de entrega pactados con sus clientes.</p>
                       <p>• Manejamos procesos transaccionales y damos seguimiento a sus envíos con un servicio de full tracking.</p>
                    </div>
                </div>
            </div>
            <div class="clearfix"> </div>
	   </div>
	   <div class="service_box1">
	   	    <div class="col-md-6">
                <div class="service_1">
                    <div class="service_1-left">
                       <span class="icon_5"><i class="fa fa-star"> </i></span>
                    </div>
                    <div class="service_1-right">
                       <h5><a href="#">Envios Personalizados</a></h5>
                       <p>• Entregas al día siguiente hasta las 10am. Servicio exclusivo a ciudades principales. Tiempos de entrega diferenciados para periféricos.</p>
                       <p>• Entregas al día siguiente máximo hasta las 12pm. Servicio exclusivo a ciudades principales. Tiempos de entrega diferenciados para periféricos.</p>
                    </div>
                </div>
            </div>
             <div class="col-md-6">
                <div class="service_1">
                    <div class="service_1-left">
                       <span class="icon_5"><i class="fa fa-globe"> </i></span>
                    </div>
                    <div class="service_1-right">
                       <h5><a href="#">Courier Express</a></h5>
                       <p>• Manejamos todo el movimiento interno de su compañía con gran flexibilidad</p>
                       <p>• Entrega prioritaria de valijas corporativas a primera hora.</p>
                       <p>• Entrega de documentos y paquetes  al día siguiente.</p>
                       <p>• Entregas el mismo día (am/pm) en las principales ciudades del país.</p>
                    </div>
                </div>
            </div>
            <div class="clearfix"> </div>
	   </div>
	</div>
	</div>
	<div class="bg_2">
	  <div class="container">
	  	 <div class="col-md-6 service_2-left">
              <h2>Profesinales en nuestro trabajo</h2>
         </div>
         <div class="col-md-6 service_2-right">
            <p></p>
         </div>
         <div class="clearfix"> </div>
	  </div>
	</div>