<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<?php 
	$username = array('autocomplete'=>'off','name' => 'Nombre', 'class'=>'text','placeholder' => 'Nombre','value' =>'','required'=>'','type'=>'text');
	$js='onfocus="this.value =\'\' "';
	$email = array('autocomplete'=>'off','name' => 'Email','class'=>'text','placeholder' => 'Email','value' => '','required'=>'','type'=>'email');
	$telefono = array('autocomplete'=>'off','name' => 'Telefono','class'=>'text','placeholder' => 'Telefono','value' => '','required'=>'','type'=>'number');
	$mensaje = array('autocomplete'=>'off','name' => 'Mensaje','class'=>'text','placeholder' => 'Mensaje','value' => '','required'=>'','type'=>'area');
	$submit=array('name' => 'submit', 'class'=>'more_btn','value' => 'Enviar Mensaje');
	$form = array('class' => 'contact_form'); 
?>
<!-- banner -->
  <div class="courses_banner">
  	<div class="container">
  		<h3>Contacto</h3>
  		<p class="description"></p>
  		<br />
        <div class="breadcrumb1">
            <ul>
                <li class="icon6"><a href="home">Home</a></li>
                <li class="current-page">Contacto</li>
            </ul>
        </div>
  	</div>
  </div>
    <!-- //banner -->
	<div class="features">
	   <div class="container">
	   	  <h1>Donde nos puedes ubicar</h1>
	   	  <div class="map">
			 <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3901.949766412419!2d-77.08306728568996!3d-12.046977345183258!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x9105c94a6a059085%3A0x58a213b3e682b1a8!2sUrbano+Peru!5e0!3m2!1ses!2spe!4v1442951848402" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe></div>
		  <div class="wrapper">
				<div class="col_1">
					<i class="fa fa-home  icon2"></i>
					<div class="box">
						<p class="marTop9">Av. Argentina 3127,<br>Lima Cercado.</p>
					</div>
				</div>

				<div class="col_2">
					<i class="fa fa-phone icon2"></i>
					<div class="box">
						<p class="marTop9">(511) 4151800<br></p>
					</div>
				</div>

				<div class="col_2">
					<i class="fa fa-envelope icon2"></i>
					<div class="box">
						<p class="m_6"><a href="mailto:comercial@urbano.com.pe" class="link4">comercial@urbano.com.pe</a></p>
					</div>
				</div>
				<div class="clearfix"> </div>
		 </div>
		 <?=form_open(base_url().'home/nuevo',$form)?>
		 	<h2>Contactenos</h2>
			<div class="col-md-6 grid_6">
				 <?=form_input($username)?>
				 <?=form_input($email)?>
				 <?=form_input($telefono)?>
			</div>
			<div class="col-md-6 grid_6">
				<?=form_textarea($mensaje)?>
			</div>
            <div class="clearfix"> </div>
            <div class="btn_3">
			  <?=form_submit($submit)?>
		    </div>
		 <?=form_close()?>			
	  </div>
	</div>