<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!-- banner -->
  <div class="courses_banner">
  	<div class="container">
  		<h3>Objetivos</h3>
  		<p class="description"></p>
  		<br />
        <div class="breadcrumb1">
            <ul>
                <li class="icon6"><a href="index.html">Home</a></li>
                <li class="current-page">Objetivos</li>
            </ul>
        </div>
  	</div>
  </div>
    <!-- //banner -->
	<div class="features">
	   <div class="container">
	   	  <h1>Objetivos</h1>
	   	  <h2>Objetivos	</h2>
	   	  <p>"Dar a las comunidades apoyo en programas educativos, ecológicos y que busquen soluciones a la pobreza.
Valorar diferentes ideas, culturas y opiniones que permitan un desarrollo pleno de nuestros clientes; colaboradores; proveedores y en general de toda la comunidad que tenga contacto con Urbano.
Lograr que la confianza de nuestros clientes, de nuestros colaboradores y la comunidad representen el norte de nuestra empresa.
Desarrollar relaciones duraderas en el tiempo que conviertan a Urbano en un verdadero socio estratégico de sus clientes.
Fomentar la innovación de productos, servicios y valores agregados para satisfacer las necesidades de nuestros clientes y las cambiantes realidades del mercado."</p>
	  </div>
	</div>