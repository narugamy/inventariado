<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!-- banner -->
  <div class="courses_banner">
  	<div class="container">
  		<h3>Historia</h3>
  		<p class="description"></p>
  		<br />
        <div class="breadcrumb1">
            <ul>
                <li class="icon6"><a href="index.html">Home</a></li>
                <li class="current-page">Historia</li>
            </ul>
        </div>
  	</div>
  </div>
    <!-- //banner -->
	<div class="features">
	   <div class="container">
	   	  <h1>Historia</h1>
	   	  <h2>Historia	</h2>
	   	  <p>"Urbano nace como una empresa de distribución postal. Esta experiencia nos ha dado la capacidad de llegar masivamente y con efectividad a millones de personas en sus casas y oficinas a diario. Gradualmente hemos expandido nuestro portafolio de servicios, productos y cobertura geográfica.

La compañía ha consolidado una posición de liderazgo en Ecuador, El Salvador y Perú.

Urbano desarrolla Soluciones Logísticas y de Inteligencia de Mercado, permitiendo a sus clientes contactarse eficientemente con los suyos, mediante herramientas innovadoras integradas en soluciones prácticas y efectivas.

Nuestro portafolio de soluciones incluye la normalización, georeferenciación, enriquecimiento, segmentación, prospección, visualización de bases de datos; impresión digital en blanco y negro y a color con información variable de imágenes y textos, al igual que las entregas físicas y/o por medios digitales, optimizando así la respuesta en campañas de Mercadeo Directo Multicanal.

Además, ofrecemos servicios logísticos que incluyen: bodegaje, distribución física e integración tecnológica para el desarrollo del comercio electrónico.

Todas estas capacidades se integran para crear soluciones diversas que responden de manera experta y especializada a necesidades que tienen las empresas en diversos giros <d></d>e negocio."</p>
	  </div>
	</div>