<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="utf-8">
    <title>Reporte de Empleados de la Empresa UrbanoExpress</title>
    <style type="text/css">
        body {
         background-color: #fff;
         margin: 40px;
         font-family: Lucida Grande, Verdana, Sans-serif;
         font-size: 14px;
         color: #4F5155;
        }

        a {
         color: #003399;
         background-color: transparent;
         font-weight: normal;
        }

        h1 {
         color: #444;
         background-color: transparent;
         border-bottom: 1px solid #D0D0D0;
         font-size: 16px;
         font-weight: bold;
         margin: 24px 0 2px 0;
         padding: 5px 0 6px 0;
        }

        h2 {
         color: #444;
         background-color: transparent;
         border-bottom: 1px solid #D0D0D0;
         font-size: 16px;
         font-weight: bold;
         margin: 24px 0 2px 0;
         padding: 5px 0 6px 0;
         text-align: center;
        }
        p{
            position: absolute;
            top:4em;
            left:11em;
            z-index:1000;
        }

        table{
            text-align: center;
        }

        /* estilos para el footer y el numero de pagina */
        @page { margin: 180px 50px; }
        #header { 
            background-color: #333; 
            color: #fff;
            height: 150px; 
            left: 0px;
            top: -180px; 
            position: fixed; 
            right: 0px; 
        }
        #footer { 
            bottom: -180px; 
            color: black;
            height: 150px; 
            left: 0px; 
            position: fixed; 
            right: 0px;
            text-align:center;
        }
        #footer .page:after { 
            content: counter(page, upper-roman);
            position: absolute;
            left: 12em;
            z-index: 5000;
        }
    </style>
</head>
<body>
    <!--header para cada pagina-->
    <div id="header">
        <p><?php echo $title ?></p>
    </div>
    <!--footer para cada pagina-->
    <div id="footer">
        <!--aqui se muestra el numero de la pagina en numeros romanos-->
        <p class="page"></p>
    </div>
    <h2>Reporte de Empleados</h2>
    <br><br>
    <table>
        <tr>
            <th width="100">Codigo</th>
            <th width="100">Datos</th>
            <th width="100">Correo</th>
            <th width="100">Dni</th>
        </tr>
        <?php
            foreach ($provincias as $per) {

            echo "<tr>";
            echo  "<td>$per->Codigo</td>";
            echo  "<td>$per->Nombre"." "."$per->Apellidos"."</td>";
            echo  "<td>$per->Email</td>";
            echo  "<td>$per->Dni</td>";
            echo "</tr>";
        } ?>  
    </table>
</body>
</html>