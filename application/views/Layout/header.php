<?php
defined('BASEPATH') OR exit('No direct script access allowed');
$token=$this->session->userdata('Perfil');?>
<!DOCTYPE HTML>
<html>
<head>
<title><?= $Titulo_Sitio;?></title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" content="Web ProyectM@ster" />
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
<link href="<?= base_url();?>assets/css/bootstrap-3.1.1.min.css" rel='stylesheet' type='text/css' />
<!-- jQuery (Necesario para BootsTrap y JavaScrip) -->
<script src="<?= base_url();?>assets/js/jquery.min.js"></script>
<script src="<?= base_url();?>assets/js/bootstrap.min.js"></script>
<!-- Custom Theme files -->
<link href="<?= base_url();?>assets/css/style.css" rel='stylesheet' type='text/css' />
<link rel="stylesheet" href="<?= base_url();?>assets/css/jquery.countdown.css" />

<link href='//fonts.googleapis.com/css?family=PT+Sans+Narrow:400,700' rel='stylesheet' type='text/css'>
<link href="<?= base_url();?>assets/css/font-awesome.css" rel="stylesheet"> 
<script>
$(document).ready(function(){
    $(".dropdown").hover(            
        function() {
            $('.dropdown-menu', this).stop( true, true ).slideDown("fast");
            $(this).toggleClass('open');        
        },
        function() {
            $('.dropdown-menu', this).stop( true, true ).slideUp("fast");
            $(this).toggleClass('open');       
        }
    );
});
</script>
</head>
<body>
<nav class="navbar navbar-default" role="navigation">
	<div class="container">
	    <div class="navbar-header">
	        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
		        <span class="sr-only">Toggle navigation</span>
		        <span class="icon-bar"></span>
		        <span class="icon-bar"></span>
		        <span class="icon-bar"></span>
	        </button>
	        <a class="navbar-brand">Urbano</a>
	    </div>
	    <!--/.navbar-header-->
	    <div class="navbar-collapse collapse" id="bs-example-navbar-collapse-1">
	        <ul class="nav navbar-nav">
	        	<?php if($this->session->userdata('Perfil')=='')
	  			{ ?>	
		        <li class="dropdown">
		            <a href="<?= base_url();?>"><i class="fa fa-user"></i><span>Login</span></a>
		        </li>
		  <?php }?>
		        <li class="dropdown">
		            <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-globe"></i><span>Spanish</span></a>
		            <ul class="dropdown-menu">
			            <li><a href="#"><span><i class="flags pe"></i><span>Spanish</span></span></a></li>
			        </ul>
		        </li>
		  <?php if(!$this->session->userdata('Perfil')==''){ 
		  	 		if($this->session->userdata('Perfil')=='ADMINISTRADOR'){
		  ?>	
		        <li class="dropdown">
		            <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-calendar"></i><span>Panel</span></a>
		             <ul class="dropdown-menu">
			            <li><a href="<?= base_url()?>empleado">Administrar Personal</a></li>
			  			<li><a href="<?= base_url()?>cargo">Administrar Cargo</a></li>
			            <li><a href="<?= base_url()?>sede">Administrar Sede</a></li>
			            <li><a href="<?= base_url()?>area">Administrar Area</a></li>
			            <li><a href="<?= base_url()?>pdf">Reporte Empleados</a></li>
		             </ul>
		        </li>
		        <?php }?>
		        <li class="dropdown">
		        	<a href="#" class="dropdown-toggle img-traba" data-toggle="dropdown"><img src="<?=base_url()?>assets/images/<?=$this->session->userdata('Img')?>" alt="usuario" id="trabajador" /></a>
		        	  <ul class="dropdown-menu">
		        	  	<li><a>Codigo: <?=$this->session->userdata('Codigo');?></a></li>
			            <li><a>Cargo: <?=$this->session->userdata('Perfil');?></a></li>
			            <li><a>Nombre: <?=$this->session->userdata('Nombre');?></a></li>
			            <li><a>Apellido: <?=$this->session->userdata('Apellidos');?></a></li>
			            <li><a>Dni: <?=$this->session->userdata('Dni');?></a></li>
			            <li><a href="<?= base_url();?>login/logout">Salir</a></li>
		              </ul>
		        </li>
		      <?php }?>	
		    </ul>
	    </div>
	    <div class="c<lear></lear>fix"> </div>
	  </div>
	    <!--/.navbar-collapse-->
</nav>
<nav class="navbar nav_bottom" role="navigation">
 <div class="container">
 <!-- Brand and toggle get grouped for better mobile display -->
  <div class="navbar-header nav_2">
      <button type="button" class="navbar-toggle collapsed navbar-toggle1" data-toggle="collapse" data-target="#bs-megadropdown-tabs">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="#"></a>
   </div> 
   <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-megadropdown-tabs">
        <ul class="nav navbar-nav nav_1">
            <li class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button">Urbano<span class="caret"></span></a>
              <ul class="dropdown-menu" role="menu">
                <li><a href="<?=base_url()?>historia">Historia</a></li>
                <li><a href="<?=base_url()?>vision">Vision</a></li>
                <li><a href="<?=base_url()?>mision">Mision</a></li>
                <li><a href="<?=base_url()?>organizacion">Organización</a></li>
                <li><a href="<?=base_url()?>objetivos">Objetivos</a></li>
              </ul>
            </li>
            <li class="active"><a href="<?= base_url()?>noticias">Noticias</a></li>
            <li><a href="<?= base_url()?>servicio">Servicio</a></li>
            <li class="last"><a href="<?=base_url()?>contacto">Contacto</a></li>
        </ul>
     </div>
     <!-- /.navbar-collapse -->
   </div>
</nav>
<!-- banner -->
	<div class="banner">
			<!-- banner Slider starts Here -->
					<script src="<?= base_url();?>assets/js/responsiveslides.min.js"></script>
					 <script>
						// You can also use "$(window).load(function() {"
						$(function () {
						  // Slideshow 4
						  $("#slider3").responsiveSlides({
							auto: true,
							pager: true,
							nav: true,
							speed: 500,
							namespace: "callbacks",
							before: function () {
							  $('.events').append("<li>before event fired.</li>");
							},
							after: function () {
							  $('.events').append("<li>after event fired.</li>");
							}
						  });
					
						});
					  </script>
					<!--//End-slider-script -->