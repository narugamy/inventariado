	    <div class="footer">
    	<div class="container">
    		<div class="col-md-3 grid_4">
    		   <h3>Resumen</h3>	
    		   <p>"URBANO ha mantenido un ritmo constante de innovación, crecimiento y expansión regional. Esta innovación satisface necesidades de empresas que buscan  disponer de una capacidad operativa y comercial eficiente. Nuestro crecimiento sostenido permite disponer de una gran cobertura geográfica y demográfica"</p>
    		      <ul class="social-nav icons_2 clearfix">
                    <li><a href="#" class="twitter"><i class="fa fa-twitter"></i></a></li>
                    <li><a href="#" class="facebook"> <i class="fa fa-facebook"></i></a></li>
                    <li><a href="#" class="google-plus"><i class="fa fa-google-plus"></i></a></li>
                 </ul>
    		</div>
    		<div class="col-md-3 grid_4">
    		   <h3>Links Rapidos</h3>
    			<ul class="footer_list">
    				<li><a href="<?=base_url()?>noticias">-  Noticas </a></li>
    				<li><a href="<?=base_url()?>historia">-  Historia</a></li>
    				<li><a href="<?=base_url()?>mision">-  Misión</a></li>
    				<li><a href="<?=base_url()?>vision">-   Visión</a></li>
    				<li><a href="<?=base_url()?>contactenos">-   Contactenos</a></li>
    			</ul>
    		</div>
    		<div class="col-md-3 grid_4">
    		   <h3>Contactenos</h3>
    			<address>
                    <strong>Direccion:</strong>
                    <br>
                    <span>Av. Argentina 3127</span>
                    <br>
                    <span>Lima Cercado.</span>
                    <br>
                    <span><strong>Mas datos:</strong></span>
                    <br>
                    <abbr>Telefono : </abbr> (511) 4151800
                    <br>
                    <abbr>Email : </abbr> <a href="mailto:comercial@urbano.com.pe">comercial@urbano.com.pe</a>
               </address>
    		</div>
    		<div class="col-md-3 grid_4">
    		   <h3>Horario</h3>
    			 <table class="table_working_hours">
		        	<tbody>
		        		<tr class="opened_1">
							<td class="day_label">Lunes</td>
							<td class="day_value">8:00 am - 6.00 pm</td>
						</tr>
					    <tr class="opened">
							<td class="day_label">Martes</td>
							<td class="day_value">8:00 am - 6.00 pm</td>
						</tr>
					    <tr class="opened">
							<td class="day_label">Miercoles</td>
							<td class="day_value">8:00 am - 6.00 pm</td>
						</tr>
					    <tr class="opened">
							<td class="day_label">Jueves</td>
							<td class="day_value">8:00 am - 6.00 pm</td>
						</tr>
					    <tr class="opened">
							<td class="day_label">Viernes</td>
							<td class="day_value">8:00 am - 6.00 pm</td>
						</tr>
					    <tr class="closed">
							<td class="day_label">Sabado</td>
							<td class="day_value closed"><span>Cerrado</span></td>
						</tr>
					    <tr class="closed">
							<td class="day_label">Domingo</td>
							<td class="day_value closed"><span>Cerrado</span></td>
						</tr>
				    </tbody>
				</table>
            </div>
    		<div class="clearfix"> </div>
    		<div class="copy">
		       <p>Copyright © 2015 Learn . Todos los derechos reservados | Design by <a href="http://www.youtube.com/channel/UCLKLzNebXrbKR9kq4x6de3Q" target="_blank">KirigayaM@ster</a> </p>
	        </div>
    	</div>
    </div>
<script src="<?= base_url();?>assets/js/jquery.countdown.js"></script>
<script src="<?= base_url();?>assets/js/script.js"></script>
</body>
</html>	