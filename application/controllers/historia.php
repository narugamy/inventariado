<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Historia extends CI_Controller {

	public function __construct()
	{
	    parent::__construct();
	}
	
	public function index()
	{	
		$data['Titulo_Sitio'] = 'Historia';
		$this->load->view('Layout/header',$data);
		$this->load->view('Static/historia');
		$this->load->view('Layout/footer');
	}
}
