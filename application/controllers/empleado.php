<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Empleado extends CI_Controller {

	public function __construct()
	{
	    parent::__construct();
	    $this->load->model('Modelempleado');
	    $this->load->model('Modelcargo');
	    $this->load->model('Modelsede');
	    $this->load->model('Modelarea');
	    
	}
	
	//Funcion Index
	public function index()
	{	
		if(!$this->session->userdata('Perfil')=='ADMINISTRADOR')
    	{
    		redirect(base_url());
    	}else{
			$data['Titulo_Sitio'] = 'Administracion de Empleados';
			$dato['visual'] = 'Dinamic/Empleado/empleado';
			$this->visual($data,$dato);
		}
	}
	
    //FUNCIÓN PARA CREAR LA MINIATURA A LA MEDIDA QUE LE DIGAMOS
    function _create_thumbnail($filename)
    {
        $config['image_library'] = 'gd2';
        //CARPETA EN LA QUE ESTÁ LA IMAGEN A REDIMENSIONAR
        $config['source_image'] = 'images/uploads/'.$filename;
        $config['create_thumb'] = TRUE;
        $config['maintain_ratio'] = TRUE;
        //CARPETA EN LA QUE GUARDAMOS LA MINIATURA
        $config['new_image']='images/uploads/thumbs/';
        $config['width'] = 150;
        $config['height'] = 150;
        $this->load->library('image_lib', $config); 
        $this->image_lib->resize();
    }

	function do_upload() 
    {
        $config['upload_path'] = './assets/images';
        $config['allowed_types'] = 'gif|jpg|png';
        $config['max_size'] = '2000';
        $config['max_width'] = '2024';
        $config['max_height'] = '2008';

        $this->load->library('upload', $config);
        //SI LA IMAGEN FALLA AL SUBIR MOSTRAMOS EL ERROR EN LA VISTA UPLOAD_VIEW
        if (!$this->upload->do_upload()) 
        {
            $data['Titulo_Sitio'] = 'Registro';
			$dato['visual'] = 'Dinamic/Empleado/register';
			$dato['error']='Error al guardarse la imagen';
			$this->visual($data,$dato);
        } else{
        	$this->load->model('Modelupload');
        //EN OTRO CASO SUBIMOS LA IMAGEN, CREAMOS LA MINIATURA Y HACEMOS 
        //ENVÍAMOS LOS DATOS AL MODELO PARA HACER LA INSERCIÓN
            $file_info = $this->upload->data();
            //USAMOS LA FUNCIÓN create_thumbnail Y LE PASAMOS EL NOMBRE DE LA IMAGEN,
            //ASÍ YA TENEMOS LA IMAGEN REDIMENSIONADA
            $this->_create_thumbnail($file_info['file_name']);
            $data = array('upload_data' => $this->upload->data());
            $imagen = $file_info['file_name'];
            $codigo=$this->input->post('Codigo');
            $subir = $this->Modelupload->subir($imagen,$codigo);
        }
    }

    public function buscar($error=NULL)
    {
    	if($this->session->userdata('Perfil')=='ADMINISTRADOR' || isset($_POST) )
    	{
    		if(!$error==null){
				$dato['error']=$error;
			}
			$dato['area1']=$this->Modelarea->get_todos();
			$dato['sede1']=$this->Modelsede->get_todos();
			$dato['cargo1']=$this->Modelcargo->cargar(null);
			$data['Titulo_Sitio'] = 'Busqueda de Empleados';
            $dato['visual'] = 'Dinamic/Empleado/buscarEmpleado';
			$this->visual($data,$dato);
        }else{
            redirect(base_url());
        }
    }

    public function buscarEmpleado()
    {
    	if(!$this->session->userdata('Perfil')=='ADMINISTRADOR')
    	{
    		redirect(base_url());
    	}else{
	    	if(!$this->input->post('Cargo')=="" || !$this->input->post('Sede')=="" || !$this->input->post('Area')=="" || (!$this->input->post('Nombre')=="" && !$this->input->post('Apellidos')==""))
	    	{
				$dato['empleado']=$this->Modelempleado->cargar(null);
	            $data['Titulo_Sitio'] = 'Busqueda de Empleados';
	            $dato['visual'] = 'Dinamic/Empleado/listaEmpleado';
	            $this->visual($data,$dato);	
			}else{
				$error="* Elija una opcion por favor";
				$this->buscar($error);
			}
		}
    }
	
	public function editar($id=NULL)
	{
		if(!$this->session->userdata('Perfil')=='ADMINISTRADOR')
    	{
    		redirect(base_url());
    	}else{
			$dato['Titulo_Sitio']='Editar Empleado';
			if($id==NULL and is_numeric($id))
			{
				echo 'Falta ID';
				return;
			}
			$vali=$this->input->post();
			if(isset($vali['Titulo']) and $this->input->post())
			{
				$this->reglas();
				if($this->form_validation->run()==TRUE)
				{
					$this->Modelempleado->edit($id);
					redirect(base_url());
				}
			}else{
				if(isset($vali['Codigo']))
				{
					if($this->Modelempleado->edit($this->input->post('Codigo'))==0)
					{
						$error="Modificacion exitosa";
					}
					$this->buscar($error);
				}else{
					$dato['datos']=$this->Modelempleado->cargar($id);
					$dato['cargo1']=$this->Modelcargo->cargar(null);
					$dato['area1']=$this->Modelarea->get_todos();
					if(empty($dato['datos']))
					{
						echo 'El id es invalido';	
					}else{
						$data['Titulo_Sitio'] = 'Busqueda de Empleados';
		            	$dato['visual'] = 'Dinamic/Empleado/editarEmpleado';
		            	$this->visual($data,$dato);
					}
				}
				
			}
		}
	}
	
	public function eliminar($id=NULL)
	{
		if(!$this->session->userdata('Perfil')=='ADMINISTRADOR')
    	{
    		redirect(base_url());
    	}else{
			$dato['Titulo_Sitio']='Eliinar Empleado';
			if($id==NULL or is_numeric($id))
			{
				echo 'No se ha encontrado un ID';
				return;
			}else{
				$this->Modelempleado->delete2($id);
				$this->Modelempleado->delete($id);
				redirect(base_url()."empleado/buscar");
			}
		}
	}
	
	public function personal()
	{
		if(!$this->session->userdata('Perfil')=='ADMINISTRADOR')
    	{
    		redirect(base_url());
    	}else{
			$dato['Titulo_Sitio']="Empleado: $id";
			if($id==NULL or is_numeric($id))
			{
				echo 'No se ha encontrado un ID';
				return;
			}else{
				$data['listado']=$this->Modelempleado->cargar($id);
				if(empty($data['listado']))
				{
					echo 'El id es invalido';	
				}else{
					$data['Titulo_Sitio'] = 'Vista Empleado';
					$dato['visual'] = 'Dinamic/Empleado/visualEmpleado';
					$this->visual($data,$dato);
				}
			}
		}
	}
	
	public function register()
	{
		if(!$this->input->post())
		{
			$this->registrar();
		}else{
			$dato=$this->input->post();
			$respuesta=$this->Modelempleado->registro();
			if(!$respuesta=="")
			{
				$error=$respuesta;
				$this->registrar($error);
			}else{
				$this->do_upload();
				$data['Titulo_Sitio'] = 'Registro Completado';
				$dato['visual'] = 'Dinamic/Empleado/register';
				$dato['nick']=$this->input->post('Codigo');
				$this->visual($data,$dato);
			}
		}
	}

    public function registrar($error=NULL)
    {   
        if($this->session->userdata('Perfil')=='ADMINISTRADOR')
        {
        	if(!$error==null){
				$dato['error']=$error;
			}
        	$dato['sede1']=$this->Modelsede->cargar('limit');
        	$dato['area1']=$this->Modelarea->cargar('limit');
        	$dato['cargo1']=$this->Modelcargo->cargar('limit');
            $data['Titulo_Sitio'] = 'Registro';
            $dato['visual'] = 'Dinamic/Empleado/register';
            $this->visual($data,$dato);
        }else{
            redirect(base_url());
        }
    }
        
	function visual($dato,$data)
	{
		//ver sentencias sql
		$this->load->view('Layout/header',$dato);
		$this->load->view($data['visual'],$data);
		$this->load->view('Layout/footer');
	}
	
}


