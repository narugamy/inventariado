<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller{

	public function __construct(){
		parent::__construct();
		$this->load->model('ModelEmpleado');
		$this->load->model('Modelcargo');
	}
	
	//Funcion index
	public function index()
	{
		switch($this->session->userdata('Perfil')){
			case '':
			$dato=array('token'=>$this->token(),'Titulo_Sitio'=>'Bienvenidos al SystemProyect');
			$data=array('visual'=>'Static/login',);
			$this->visual($dato,$data);
			break;
			case 'ADMINISTRADOR':
			redirect(base_url().'administrador');
			break;
			default:
			redirect(base_url().'usuario');
			break;		
		}
	}
	
	//Funcion Nueva cuenta
	public function nuevo(){
		if($this->input->post()==NULL || !$this->input->post()){
			redirect(base_url());
		}
		if($this->input->post('token') && $this->input->post('token') == $this->session->userdata('token')){
			$this->reglas('');
			if($this->form_validation->run() == FALSE){
				$this->index();
			}else{
				$username = $this->input->post('log_username');
				$password = $this->input->post('Password');
				$check_user = $this->ModelEmpleado->login($username,$password);
				if($check_user=="usuario o contraseña incorrecto"){
					$data['token'] = $this->token();
					$dato['Titulo_Sitio'] = 'Bienvenidos al SystemProyect';
					$data['visual'] = 'Static/login';
					$data['error'] = $check_user;
					$this->visual($dato,$data);
				}else{
					$cod=$check_user->CodigoCargo;
					$dato=$this->Modelcargo->cargo($cod);
					if($check_user == TRUE){
						$data = array(
							'is_logued_in' 	=> 		TRUE,
							'Perfil'		=>		$dato->Nombre,
							'Nombre'		=>		$check_user->Nombre,
							'Apellidos'		=>		$check_user->Apellidos,
							'Codigo'	 	=> 		$check_user->Codigo,
							'Dni' 			=> 		$check_user->Dni,
							'Img'			=>		$check_user->Ruta
						);
						$this->session->set_userdata($data);
						$this->index();
					}
				}
			}
		}else{
			redirect(base_url());
		}
	}
	

	//Reglas de validacion
	function reglas($dato){
		$this->form_validation->set_rules('log_username', 'nombre de usuario', 'trim|min_length[2]|max_length[150]|required');
		$this->form_validation->set_rules('Password', 'password', 'trim|min_length[4]|max_length[150]|required');
		if($dato=='Register'){
			$this->form_validation->set_rules('Nombre','Nombre','trim|min_length[9]|max_length[30]|required');
		}
	}
	
	//Crear nuevo token
	public function token(){
		$token = md5(uniqid(rand(),true));
		$this->session->set_userdata('token',$token);
		return $token;
	}
	

	//Funcion para visualisacion de las vistas
	function visual($dato,$data){
		$this->load->view('Layout/header',$dato);
		$this->load->view($data['visual'],$data);
		$this->load->view('Layout/footer');
	}

}
