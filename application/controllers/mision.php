<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mision extends CI_Controller {

	public function __construct()
	{
	    parent::__construct();
	}
	
	public function index()
	{	
		$data['Titulo_Sitio'] = 'Mision';
		$this->load->view('Layout/header',$data);
		$this->load->view('Static/mision');
		$this->load->view('Layout/footer');
	}
}
