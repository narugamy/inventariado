<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Area extends CI_Controller {

	public function __construct()
	{
	    parent::__construct();
	    $this->load->model('Modelarea');
	    
	}
	
	public function index()
	{	
		if(!$this->session->userdata('Perfil')=='ADMINISTRADOR')
    	{
    		redirect(base_url());
    	}else{
			$data['Titulo_Sitio'] = 'Administracion de Areas';
			$dato['visual'] = 'Dinamic/area/area';
			$this->visual($data,$dato);
		}
	}
	
    public function buscar($error=NULL)
    {
    	if($this->session->userdata('Perfil')=='ADMINISTRADOR')
    	{
    		if(!$error==null){
				$dato['error']=$error;
			}
			$data['Titulo_Sitio'] = 'Busqueda de Areas';
            $dato['visual'] = 'Dinamic/area/buscarArea';
			$dato['area1']=$this->Modelarea->cargar(null);
			$this->visual($data,$dato);
        }else{
            redirect(base_url());
        }
    }

    public function buscararea()
    {
    	if(!$this->session->userdata('Perfil')=='ADMINISTRADOR' || $this->input->post()==NULL)
    	{
    		redirect(base_url());
    	}else{
				$dato['area']=$this->Modelarea->cargar(null);
	            $data['Titulo_Sitio'] = 'Busqueda de Area';
	            $dato['visual'] = 'Dinamic/area/listaArea';
	            $this->visual($data,$dato);
		}
    }
	
	public function editar($id=NULL)
	{
		if(!$this->session->userdata('Perfil')=='ADMINISTRADOR')
    	{
    		redirect(base_url());
    	}else{
			$dato['Titulo_Sitio']='Editar Area';
			if($this->input->post())
			{
				$vali=$this->input->post();
				$id=$vali['CodigoArea'];
			}
			if($id==NULL or !is_numeric($id))
			{
				echo 'Falta ID';
			}else{
				if(isset($vali['CodigoArea']))
				{
					$this->Modelarea->edit($id);
					$error="Modificacion exitosa";
					$this->buscar($error);
				}else{
					$dato['area1']=$this->Modelarea->cargar($id);
					if(empty($dato['area1']))
					{
						echo 'El id es invalido';	
					}else{
						$data['Titulo_Sitio'] = 'Busqueda de Areas';
		            	$dato['visual'] = 'Dinamic/area/editArarea';
		            	$this->visual($data,$dato);
					}	
				}
			}
		}
	}
	
	public function eliminar($id=NULL)
	{
		if(!$this->session->userdata('Perfil')=='ADMINISTRADOR')
    	{
    		redirect(base_url());
    	}else{
			$dato['Titulo_Sitio']='Eliinar Area';
			if($id==NULL or !is_numeric($id))
			{
				echo 'No se ha encontrado un ID';
				return;
			}else{
				$this->Modelarea->delete($id);
				$this->buscar("Eliminacion Existosa");
			}
		}
	}
	
		public function habilitar($id=NULL)
	{
		if(!$this->session->userdata('Perfil')=='ADMINISTRADOR')
    	{
    		redirect(base_url());
    	}else{
			$dato['Titulo_Sitio']='Habilitar Area';
			if($id==NULL or !is_numeric($id))
			{
				echo 'No se ha encontrado un ID';
				return;
			}else{
				$this->Modelarea->habilitar($id);
				$this->buscar("Habilitacion Existosa");
			}
		}
	}
	
	public function area($id)
	{
		if(!$this->session->userdata('Perfil')=='ADMINISTRADOR')
    	{
    		redirect(base_url());
    	}else{
			$dato['Titulo_Sitio']="Area : $id";
			if($id==NULL or !is_numeric($id))
			{
				echo 'No se ha encontrado un ID';
				return;
			}else{
				$data['listado']=$this->Modelarea->cargar($id);
				if(empty($data['listado']))
				{
					echo 'El id es invalido';	
				}else{
					$data['Titulo_Sitio'] = 'Vista Area';
					$dato['visual'] = 'Dinamic/area/visualArea';
					$this->visual($data,$dato);
				}
			}
		}
	}
	
	public function register()
	{
		if(!$this->input->post())
		{
			$this->registrar();
		}else{
			$dato=$this->input->post();
			$respuesta=$this->Modelarea->registro();
			if(!$respuesta=="")
			{
				$error=$respuesta;
				$this->registrar($error);
			}else{
				$data['Titulo_Sitio'] = 'Registro Completado';
				$dato['visual'] = 'Dinamic/area/register';
				$dato['error']="Registro exitoso";
				$this->visual($data,$dato);
			}
		}
	}

    public function registrar($error=NULL)
    {   
        if($this->session->userdata('Perfil')=='ADMINISTRADOR')
        {
        	if(!$error==null){
				$dato['error']=$error;
			}
            $data['Titulo_Sitio'] = 'Registro';
            $dato['visual'] = 'Dinamic/area/register';
            $this->visual($data,$dato);
        }else{
            redirect(base_url());
        }
    }
        
	function visual($dato,$data)
	{
		//ver sentencias sql
		$this->load->view('Layout/header',$dato);
		$this->load->view($data['visual'],$data);
		$this->load->view('Layout/footer');
	}
	
}