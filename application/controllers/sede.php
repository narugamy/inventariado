<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Sede extends CI_Controller {

	public function __construct()
	{
	    parent::__construct();
	    $this->load->model('Modelsede');
	    
	}
	
	public function index()
	{	
		if(!$this->session->userdata('Perfil')=='ADMINISTRADOR')
    	{
    		redirect(base_url());
    	}else{
			$data['Titulo_Sitio'] = 'Administracion de Sedes';
			$dato['visual'] = 'Dinamic/Sede/sede';
			$this->visual($data,$dato);
		}
	}
	
    public function buscar($error=NULL)
    {
    	if($this->session->userdata('Perfil')=='ADMINISTRADOR')
    	{
    		if(!$error==null){
				$dato['error']=$error;
			}
			$data['Titulo_Sitio'] = 'Busqueda de Sedes';
            $dato['visual'] = 'Dinamic/Sede/buscarSede';
			$dato['sede1']=$this->Modelsede->cargar(null);
			$this->visual($data,$dato);
        }else{
            redirect(base_url());
        }
    }

    public function buscarSede()
    {
    	if(!$this->session->userdata('Perfil')=='ADMINISTRADOR' || $this->input->post()==NULL)
    	{
    		redirect(base_url());
    	}else{
				$dato['sede']=$this->Modelsede->cargar(null);
	            $data['Titulo_Sitio'] = 'Busqueda de Sede';
	            $dato['visual'] = 'Dinamic/Sede/listaSede';
	            $this->visual($data,$dato);
		}
    }
	
	public function editar($id=NULL)
	{
		if(!$this->session->userdata('Perfil')=='ADMINISTRADOR')
    	{
    		redirect(base_url());
    	}else{
			$dato['Titulo_Sitio']='Editar Sede';
			if($this->input->post())
			{
				$vali=$this->input->post();
				$id=$vali['CodigoSede'];
			}
			if($id==NULL or !is_numeric($id))
			{
				echo 'Falta ID';
			}else{
				if(isset($vali['CodigoSede']))
				{
					$this->Modelsede->edit($id);
					$error="Modificacion exitosa";
					$this->buscar($error);
				}else{
					$dato['sede1']=$this->Modelsede->cargar($id);
					if(empty($dato['sede1']))
					{
						echo 'El id es invalido';	
					}else{
						$data['Titulo_Sitio'] = 'Busqueda de Sedes';
		            	$dato['visual'] = 'Dinamic/Sede/editarSede';
		            	$this->visual($data,$dato);
					}	
				}
			}
		}
	}
	
	public function eliminar($id=NULL)
	{
		if(!$this->session->userdata('Perfil')=='ADMINISTRADOR')
    	{
    		redirect(base_url());
    	}else{
			$dato['Titulo_Sitio']='Eliinar Sede';
			if($id==NULL or !is_numeric($id))
			{
				echo 'No se ha encontrado un ID';
				return;
			}else{
				$this->Modelsede->delete($id);
				$this->buscar("Eliminacion Existosa");
			}
		}
	}
	
		public function habilitar($id=NULL)
	{
		if(!$this->session->userdata('Perfil')=='ADMINISTRADOR')
    	{
    		redirect(base_url());
    	}else{
			$dato['Titulo_Sitio']='Habilitar Sede';
			if($id==NULL or !is_numeric($id))
			{
				echo 'No se ha encontrado un ID';
				return;
			}else{
				$this->Modelsede->habilitar($id);
				$this->buscar("Habilitacion Existosa");
			}
		}
	}
	
	public function sede($id)
	{
		if(!$this->session->userdata('Perfil')=='ADMINISTRADOR')
    	{
    		redirect(base_url());
    	}else{
			$dato['Titulo_Sitio']="Sede : $id";
			if($id==NULL or !is_numeric($id))
			{
				echo 'No se ha encontrado un ID';
				return;
			}else{
				$data['listado']=$this->Modelsede->cargar($id);
				if(empty($data['listado']))
				{
					echo 'El id es invalido';	
				}else{
					$data['Titulo_Sitio'] = 'Vista Sede';
					$dato['visual'] = 'Dinamic/Sede/visualSede';
					$this->visual($data,$dato);
				}
			}
		}
	}
	
	public function register()
	{
		if(!$this->input->post())
		{
			$this->registrar();
		}else{
			$dato=$this->input->post();
			$respuesta=$this->Modelsede->registro();
			if(!$respuesta=="")
			{
				$error=$respuesta;
				$this->registrar($error);
			}else{
				$data['Titulo_Sitio'] = 'Registro Completado';
				$dato['visual'] = 'Dinamic/Sede/register';
				$dato['error']="Registro exitoso";
				$this->visual($data,$dato);
			}
		}
	}

    public function registrar($error=NULL)
    {   
        if($this->session->userdata('Perfil')=='ADMINISTRADOR')
        {
        	if(!$error==null){
				$dato['error']=$error;
			}
            $data['Titulo_Sitio'] = 'Registro';
            $dato['visual'] = 'Dinamic/Sede/register';
            $this->visual($data,$dato);
        }else{
            redirect(base_url());
        }
    }
        
	function visual($dato,$data)
	{
		//ver sentencias sql
		$this->load->view('Layout/header',$dato);
		$this->load->view($data['visual'],$data);
		$this->load->view('Layout/footer');
	}
	
}


