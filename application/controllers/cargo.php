<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cargo extends CI_Controller {

	public function __construct()
	{
	    parent::__construct();
	    $this->load->model('Modelcargo');
	    
	}
	
	public function index()
	{	
		if(!$this->session->userdata('Perfil')=='ADMINISTRADOR')
    	{
    		redirect(base_url());
    	}else{
			$data['Titulo_Sitio'] = 'Administracion de Cargos';
			$dato['visual'] = 'Dinamic/Cargo/cargo';
			$this->visual($data,$dato);
		}
	}
	
    public function buscar($error=NULL)
    {
    	if($this->session->userdata('Perfil')=='ADMINISTRADOR')
    	{
    		if(!$error==null){
				$dato['error']=$error;
			}
			$data['Titulo_Sitio'] = 'Busqueda de Cargos';
            $dato['visual'] = 'Dinamic/Cargo/buscarCargo';
			$this->visual($data,$dato);
        }else{
            redirect(base_url());
        }
    }

    public function buscarCargo()
    {
    	if(!$this->session->userdata('Perfil')=='ADMINISTRADOR' || $this->input->post()==NULL)
    	{
    		redirect(base_url());
    	}else{
				$dato['cargo']=$this->Modelcargo->cargar(null);
	            $data['Titulo_Sitio'] = 'Busqueda de Cargo';
	            $dato['visual'] = 'Dinamic/Cargo/listaCargo';
	            $this->visual($data,$dato);
		}
    }
	
	public function editar($id=NULL)
	{
		if(!$this->session->userdata('Perfil')=='ADMINISTRADOR')
    	{
    		redirect(base_url());
    	}else{
			$dato['Titulo_Sitio']='Editar Cargo';
			if($this->input->post())
			{
				$vali=$this->input->post();
				$id=$vali['CodigoCargo'];
			}
			if($id==NULL or !is_numeric($id))
			{
				echo 'Falta ID';
			}else{
				if(isset($vali['CodigoCargo']))
				{
					$this->Modelcargo->edit($id);
					$error="Modificacion exitosa";
					$this->buscar($error);
				}else{
					$dato['cargo1']=$this->Modelcargo->cargar($id);
					if(empty($dato['cargo1']))
					{
						echo 'El id es invalido';	
					}else{
						$data['Titulo_Sitio'] = 'Busqueda de Cargos';
		            	$dato['visual'] = 'Dinamic/Cargo/editarCargo';
		            	$this->visual($data,$dato);
					}	
				}
			}
		}
	}
	
	public function eliminar($id=NULL)
	{
		if(!$this->session->userdata('Perfil')=='ADMINISTRADOR')
    	{
    		redirect(base_url());
    	}else{
			$dato['Titulo_Sitio']='Eliinar Cargo';
			if($id==NULL or !is_numeric($id))
			{
				echo 'No se ha encontrado un ID';
				return;
			}else{
				$this->Modelcargo->delete($id);
				$this->buscar("Eliminacion Existosa");
			}
		}
	}
	
		public function habilitar($id=NULL)
	{
		if(!$this->session->userdata('Perfil')=='ADMINISTRADOR')
    	{
    		redirect(base_url());
    	}else{
			$dato['Titulo_Sitio']='Habilitar Cargo';
			if($id==NULL or !is_numeric($id))
			{
				echo 'No se ha encontrado un ID';
				return;
			}else{
				$this->Modelcargo->habilitar($id);
				$this->buscar("Habilitacion Existosa");
			}
		}
	}
	
	public function cargo($id)
	{
		if(!$this->session->userdata('Perfil')=='ADMINISTRADOR')
    	{
    		redirect(base_url());
    	}else{
			$dato['Titulo_Sitio']="Cargo : $id";
			if($id==NULL or !is_numeric($id))
			{
				echo 'No se ha encontrado un ID';
				return;
			}else{
				$data['listado']=$this->Modelcargo->cargar($id);
				if(empty($data['listado']))
				{
					echo 'El id es invalido';	
				}else{
					$data['Titulo_Sitio'] = 'Vista Cargo';
					$dato['visual'] = 'Dinamic/Cargo/visualCargo';
					$this->visual($data,$dato);
				}
			}
		}
	}
	
	public function register()
	{
		if(!$this->input->post())
		{
			$this->registrar();
		}else{
			$dato=$this->input->post();
			$respuesta=$this->Modelcargo->registro();
			if(!$respuesta=="")
			{
				$error=$respuesta;
				$this->registrar($error);
			}else{
				$data['Titulo_Sitio'] = 'Registro Completado';
				$dato['visual'] = 'Dinamic/Cargo/register';
				$dato['error']="Registro exitoso";
				$this->visual($data,$dato);
			}
		}
	}

    public function registrar($error=NULL)
    {   
        if($this->session->userdata('Perfil')=='ADMINISTRADOR')
        {
        	if(!$error==null){
				$dato['error']=$error;
			}
            $data['Titulo_Sitio'] = 'Registro';
            $dato['visual'] = 'Dinamic/Cargo/register';
            $this->visual($data,$dato);
        }else{
            redirect(base_url());
        }
    }
        
	function visual($dato,$data)
	{
		//ver sentencias sql
		$this->load->view('Layout/header',$dato);
		$this->load->view($data['visual'],$data);
		$this->load->view('Layout/footer');
	}
	
}


