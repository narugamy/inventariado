<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Noticias extends CI_Controller {

	public function __construct()
	{
	    parent::__construct();
	}
	
	public function index()
	{	
		$data['Titulo_Sitio'] = 'Noticias';
		$data['body']="urbano";
		$data['body2']="noticias";
		$this->load->view('Layout/header',$data);
		$this->load->view('Static/noticias');
		$this->load->view('Layout/footer');
	}
}
