 <?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller 
{

	public function __construct()
	{
	    parent::__construct(); 
	    $this->load->model('Modelempleado');
	    $this->load->model('Modelupload');
	}
	
	public function index()
	{	
		$data['Titulo_Sitio'] = 'Login';
		$dato['visual'] = 'Static/login';
		$this->visual($data,$dato);
	}
	
	public function logout()
	{
		$this->session->sess_destroy();
		redirect(base_url());
		$this->index();
	}
	
	public function terminos()
	{
		$data['Titulo_Sitio']='Terminos y condiciones';
		$dato['visual'] = 'Static/terminos';
		$this->visual($data,$dato);
	}
	
	function visual($data,$dato)
	{
		$this->load->view('Layout/header',$data);
		$this->load->view($dato['visual'],$dato);	
		$this->load->view('Layout/footer');
	}
	
}
