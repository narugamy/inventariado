<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Usuario extends CI_Controller {
	
	public function __construct(){
        parent::__construct();
		$this->load->model('ModelUsuario');
    }

    public function index(){
		$data['Titulo_Sitio'] = 'Noticias';
		$this->load->view('Layout/header',$data);
		$this->load->view('Static/noticias');
		$this->load->view('Layout/footer');
    }
}
