<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Organizacion extends CI_Controller {

	public function __construct()
	{
	    parent::__construct();
	}
	
	public function index()
	{	
		$data['Titulo_Sitio'] = 'Estructura de la Empresa';
		$this->load->view('Layout/header',$data);
		$this->load->view('Static/organizacion');
		$this->load->view('Layout/footer');
	}
}
