<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Objetivos extends CI_Controller {

	public function __construct()
	{
	    parent::__construct();
	}
	
	public function index()
	{	
		$data['Titulo_Sitio'] = 'Objetivos';
		$this->load->view('Layout/header',$data);
		$this->load->view('Static/objetivos');
		$this->load->view('Layout/footer');
	}
}
