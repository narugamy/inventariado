<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Administrador extends CI_Controller 
{
	
	public function __construct()
    {
        parent::__construct();
    }

    public function index()
    {
    	if(!$this->session->userdata('Perfil')=='ADMINISTRADOR')
    	{
    		redirect(base_url());
    	}else{
    		$data['Titulo_Sitio'] = 'Noticias';
			$dato['visual'] = 'Static/noticias';
			$this->visual($data,$dato);
		}
    }
    
	function visual($dato,$data)
	{
		$this->load->view('Layout/header',$dato);
		$this->load->view($data['visual'],$data);
		$this->load->view('Layout/footer');
	}
	
	
	
}
