<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Vision extends CI_Controller {

	public function __construct()
	{
	    parent::__construct();
	}
	
	public function index()
	{	
		$data['Titulo_Sitio'] = 'Visión';
		$this->load->view('Layout/header',$data);
		$this->load->view('Static/vision');
		$this->load->view('Layout/footer');
	}
}
