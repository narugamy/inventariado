<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Contacto extends CI_Controller {

	public function __construct()
	{
	    parent::__construct();
	}
	
	public function index()
	{	
		$data['Titulo_Sitio'] = 'Contactanos';
		$this->load->view('Layout/header',$data);
		$this->load->view('Static/contacto');
		$this->load->view('Layout/footer');
	}
}
