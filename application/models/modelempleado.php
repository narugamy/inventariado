<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class Modelempleado extends CI_Model {
	
	public function login($username,$password)
	{ 
		$username=$this->db->escape_str($username);
		$password=md5($this->db->escape_str($password));
		$this->db->where('Codigo',$username);
		$this->db->where('Password',$password);
		$query = $this->db->get('empleado');
		if($query->num_rows() == 1)
		{
			return $query->row();
		}else{
			return 'usuario o contraseña incorrecto';
			
		}
	}
	public function cargo($Cod)
	{
		$this->db->where('Codigo',$Cod);
		$query = $this->db->get('Codigo');
		return $query->result();
	}
	
	public function verificar($dato)
	{
		$this->db->where('Codigo',$dato['Codigo']);
		$query = $this->db->get('empleado');
		
		if($query->num_rows() == 1)
		{
			return 1;
		}else{
			return 0;
		}
	}
	
	public function registro()
	{
		$dato=$this->input->post();
		$ver=$this->verificar($dato);
		if($ver==1)
		{
			return 'Algunos datos ya existen';
		}else{
			unset($dato['Enviar'],$dato['remember']);
			$respuesta=$this->db->insert('empleado',$dato);
			return '';	
		}
	}
	
	public function registro2($sede,$area)
	{
		$dato['CodigoArea']=$area;
		$dato['CodigoSede']=$sede;
		$respuesta=$this->db->insert('area_sede',$dato);
		return $respuesta;	
	}
	
	function delete($id)
	{
		$this->db->where('Codigo',$id);
		$this->db->delete('empleado');
	}
	
	function delete2($id)
	{
		$this->db->where('Codigo',$id);
		$this->db->delete('area_sede_empleado');
	}
	
	function edit($id)
	{
		$data_inset=$this->input->post();
		unset($data_inset['remember']);
		$this->db->where('Codigo',$id);
		$this->db->update('empleado',$data_inset);
		return 0;
	}
	
	public function cargar($id)
	{
		if($id==null)
		{
			if($this->input->post('Apellidos')=="" || $this->input->post('Nombre')=="")
			{
				if(!$this->input->post('Cargo')=="")
				{
					$this->db->where('CodigoCargo',$this->input->post('Cargo'));
					$Sql=$this->db->get('empleado');
				}else if(!$this->input->post('Sede')=="")
				{
					$this->db->where('CodigoSede',$this->input->post('Sede'));
					$Sql=$this->db->get('empleado');
				}else if(!$this->input->post('Area')=="")
				{
					$this->db->where('CodigoArea',$this->input->post('Area'));
					$Sql=$this->db->get('empleado');
				}
			}else{
				$this->db->like('Nombre',$this->input->post('Nombre'));
				$this->db->or_like('Apellidos',$this->input->post('Apellidos'));
				$Sql=$this->db->get('empleado');
			}
		}else{
			$this->db->where('Codigo',$id);
			$Sql=$this->db->get('empleado');
		}
		return $Sql->result();	
	}
	
}