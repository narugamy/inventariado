<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');
 
class Modelsede extends CI_Model 
{

    function get_todos()
	{
		$Sql=$this->db->get('sede');
		return $Sql->result();
	}
	
	public function verificar($dato)
	{
		$this->db->where('Nombre',$dato['Nombre']);
		$query = $this->db->get('sede');
		if($query->num_rows() == 1)
		{
			return 1;
		}else{
			return 0;
		}
	}

	public function cargo($cod)
	{
		$this->db->where('CodigoCargo',$cod);
		$query = $this->db->get('cargo');
		return $query->row();	
	}
	
	public function cargar($id)
	{
		if($id==null)
		{
			if($this->input->post('Nombre')=="")
			{
				$Sql=$this->db->get('sede');
			}else{
				$this->db->where('Nombre',$this->input->post('Nombre'));
				$Sql=$this->db->get('sede');
			}
		}elseif($id=="limit"){
				$Sql=$this->db->where('Estado','1');
			$Sql=$this->db->get('sede');
		}else{
			$Sql=$this->db->where('CodigoSede',$id);
			$Sql=$this->db->get('sede');
		}
		return $Sql->result();	
	}
	
	public function registro()
	{
		$dato=$this->input->post();
		unset($dato['remember']);
		$ver=$this->verificar($dato);
		if($ver==1)
		{
			return 'El cargo ya existe';
		}else{
			$respuesta=$this->db->insert('cargo',$dato);
			$val=$this->db->insert_id();
			$this->ordenar($val);
			return '';	
		}
	}
	
	function ordenar($valor)
	{
		$this->load->model('Modelarea');
		$array=$this->Modelarea->get_todos();
		$dato['CodigoSede']=$valor;
		foreach($array as $sede)
        {
        	$dato['CodigoArea']=$sede->CodigoArea;
        	$this->db->insert('area_sede',$dato);
        }
	}
	
	public function edit($id)
	{
		$data_inset=$this->input->post();
		unset($data_inset['Enviar'],$data_inset['remember'],$data_inset['CodigoSede']);
		$this->db->where('CodigoSede',$id);
		$this->db->update('sede',$data_inset);
	}
    
    public function delete($id)
	{
		$dato['Estado']=0;
		$this->db->where('CodigoSede',$id);
		$this->db->update('sede',$dato);
	}
	
	public function habilitar($id)
	{
		$dato['Estado']=1;
		$this->db->where('CodigoSede',$id);
		$this->db->update('sede',$dato);
	}
	
}