<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');
 
class Modelarea extends CI_Model 
{

    function get_todos()
	{
		$Sql=$this->db->get('area');
		return $Sql->result();
	}
	
	public function verificar($dato)
	{
		$this->db->where('Nombre',$dato['Nombre']);
		$query = $this->db->get('area');
		if($query->num_rows() == 1)
		{
			return 1;
		}else{
			return 0;
		}
	}

	public function cargo($cod)
	{
		$this->db->where('CodigoArea',$cod);
		$query = $this->db->get('area');
		return $query->row();	
	}
	
	public function cargar($id)
	{
		if($id==null)
		{
			if($this->input->post('Nombre')=="")
			{
				$Sql=$this->db->get('area');
			}else{
				$this->db->like('Nombre',$this->input->post('Nombre'));
				$Sql=$this->db->get('area');
			}
		}elseif($id=="limit"){
				$Sql=$this->db->where('Estado','1');
			$Sql=$this->db->get('area');
		}else{
			$Sql=$this->db->where('CodigoArea',$id);
			$Sql=$this->db->get('area');
		}
		return $Sql->result();	
	}
	
	public function registro()
	{	
		$dato=$this->input->post();
		unset($dato['remember']);
		$ver=$this->verificar($dato);
		if($ver==1)
		{
			return 'El area ya existe';
		}else{
			$this->db->insert('area',$dato);
			$val=$this->db->insert_id();
			$this->ordenar($val);
			return '';	
		}
	}
	
	function ordenar($valor)
	{
		$this->load->model('Modelsede');
		$array=$this->Modelsede->get_todos();
		$dato['CodigoArea']=$valor;
		foreach($array as $sede)
        {
        	$dato['CodigoSede']=$sede->CodigoSede;
        	$this->db->insert('area_sede',$dato);
        }
	}
	
	public function edit($id)
	{
		$data_inset=$this->input->post();
		unset($data_inset['Enviar'],$data_inset['remember'],$data_inset['CodigoArea']);
		$this->db->where('CodigoArea',$id);
		$this->db->update('area',$data_inset);
	}
    
    public function delete($id)
	{
		$dato['Estado']=0;
		$this->db->where('CodigoArea',$id);
		$this->db->update('area',$dato);
	}
	
	public function habilitar($id)
	{
		$dato['Estado']=1;
		$this->db->where('CodigoArea',$id);
		$this->db->update('area',$dato);
	}
	
}