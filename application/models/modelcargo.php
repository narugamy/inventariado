	<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');
 
class Modelcargo extends CI_Model 
{

    function get_todos()
	{
		$Sql=$this->db->get('cargo');
		return $Sql->result();
	}
	
	public function verificar($dato)
	{
		$this->db->where('Nombre',$dato['Nombre']);
		$query = $this->db->get('cargo');
		if($query->num_rows() == 1)
		{
			return 1;
		}else{
			return 0;
		}
	}

	public function cargo($cod)
	{
		$this->db->where('CodigoCargo',$cod);
		$query = $this->db->get('cargo');
		return $query->row();	
	}
	
	public function cargar($id)
	{
		if($id==null)
		{
			if($this->input->post('Nombre')=="")
			{
				$Sql=$this->db->get('cargo');
			}else{
				$this->db->like('Nombre',$this->input->post('Nombre'));
				$Sql=$this->db->get('cargo');
			}
		}elseif($id=="limit"){
				$Sql=$this->db->where('Estado','1');
			$Sql=$this->db->get('cargo');
		}else{
			$Sql=$this->db->where('CodigoCargo',$id);
			$Sql=$this->db->get('cargo');
		}
		return $Sql->result();	
	}
	
	public function registro()
	{
		$dato=$this->input->post();
		unset($dato['remember']);
		$ver=$this->verificar($dato);
		if($ver==1)
		{
			return 'El cargo ya existe';
		}else{
			$respuesta=$this->db->insert('cargo',$dato);
			return '';	
		}
	}
	
	public function edit($id)
	{
		$data_inset=$this->input->post();
		unset($data_inset['Enviar'],$data_inset['remember'],$data_inset['CodigoCargo']);
		$this->db->where('CodigoCargo',$id);
		$this->db->update('cargo',$data_inset);
	}
    
    public function delete($id)
	{
		$dato['Estado']=0;
		$this->db->where('CodigoCargo',$id);
		$this->db->update('cargo',$dato);
	}
	
	public function habilitar($id)
	{
		$dato['Estado']=1;
		$this->db->where('CodigoCargo',$id);
		$this->db->update('cargo',$dato);
	}
	
}